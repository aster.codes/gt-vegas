--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5 (Ubuntu 11.5-0ubuntu0.19.04.1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-0ubuntu0.19.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: qotd; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qotd (
    id serial PRIMARY KEY,
    question text NOT NULL,
    author character varying(255),
    date_added date NOT NULL,
    date_posted date NOT NULL,
    archived boolean DEFAULT false
);


ALTER TABLE public.qotd OWNER TO postgres;

--
-- Name: qotd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

--CREATE SEQUENCE public.qotd_id_seq
--    AS integer
--    START WITH 1
--    INCREMENT BY 1
--    NO MINVALUE
--    NO MAXVALUE
--    CACHE 1;
--

ALTER TABLE public.qotd_id_seq OWNER TO postgres;

--
-- Name: qotd_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.qotd_id_seq OWNED BY public.qotd.id;


--
-- Name: qotd id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qotd ALTER COLUMN id SET DEFAULT nextval('public.qotd_id_seq'::regclass);


--
-- PostgreSQL database dump complete
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: qotd_qotdanswer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.qotd_qotdanswer (
    id integer NOT NULL,
    qotd_id integer NOT NULL,
    content text NOT NULL,
    avatar_url character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    channel_id character varying(255) NOT NULL,
    message_id character varying(255) NOT NULL
);


ALTER TABLE public.qotd_qotdanswer OWNER TO postgres;

--
-- Name: qotd_qotdanswer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.qotd_qotdanswer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qotd_qotdanswer_id_seq OWNER TO postgres;

--
-- Name: qotd_qotdanswer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.qotd_qotdanswer_id_seq OWNED BY public.qotd_qotdanswer.id;


--
-- Name: qotd_qotdanswer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qotd_qotdanswer ALTER COLUMN id SET DEFAULT nextval('public.qotd_qotdanswer_id_seq'::regclass);


--
-- Name: qotd_qotdanswer qotd_qotdanswer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qotd_qotdanswer
    ADD CONSTRAINT qotd_qotdanswer_pkey PRIMARY KEY (id);


--
-- Name: qotd_qotdanswer_qotd_id_e0d67e40; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX qotd_qotdanswer_qotd_id_e0d67e40 ON public.qotd_qotdanswer USING btree (qotd_id);


--
-- Name: qotd_qotdanswer qotd_qotdanswer_qotd_id_e0d67e40_fk_qotd_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.qotd_qotdanswer
    ADD CONSTRAINT qotd_qotdanswer_qotd_id_e0d67e40_fk_qotd_id FOREIGN KEY (qotd_id) REFERENCES public.qotd(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: TABLE qotd_qotdanswer; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.qotd_qotdanswer TO postgres;


--
-- PostgreSQL database dump complete
--

