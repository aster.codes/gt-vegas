--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: wotd_wotd; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wotd_wotd (
    id integer NOT NULL,
    word character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    xp_reward character varying(255) NOT NULL,
    multiplier_reward character varying(255) NOT NULL,
    date_added date NOT NULL,
    date_for date,
    date_found timestamp with time zone,
    archived boolean NOT NULL,
    guild bigint NOT NULL
);


ALTER TABLE public.wotd_wotd OWNER TO postgres;

--
-- Name: wotd_wotd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.wotd_wotd_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wotd_wotd_id_seq OWNER TO postgres;

--
-- Name: wotd_wotd_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.wotd_wotd_id_seq OWNED BY public.wotd_wotd.id;


--
-- Name: wotd_wotd id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wotd_wotd ALTER COLUMN id SET DEFAULT nextval('public.wotd_wotd_id_seq'::regclass);


--
-- Name: wotd_wotd wotd_wotd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wotd_wotd
    ADD CONSTRAINT wotd_wotd_pkey PRIMARY KEY (id);


--
-- Name: wotd_wotd wotd_wotd_word_90c98f75_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wotd_wotd
    ADD CONSTRAINT wotd_wotd_word_90c98f75_uniq UNIQUE (word);


--
-- Name: wotd_wotd_guild_1e8407a4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wotd_wotd_guild_1e8407a4 ON public.wotd_wotd USING btree (guild);


--
-- Name: wotd_wotd_word_90c98f75_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX wotd_wotd_word_90c98f75_like ON public.wotd_wotd USING btree (word varchar_pattern_ops);


--
-- PostgreSQL database dump complete
--

