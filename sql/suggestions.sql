--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-0ubuntu0.20.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: suggestions_suggestion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.suggestions_suggestion (
    id integer NOT NULL,
    suggestion text NOT NULL,
    attachment character varying(255),
    submitted timestamp with time zone NOT NULL,
    discord_user_id integer NOT NULL,
    approved boolean
);


ALTER TABLE public.suggestions_suggestion OWNER TO postgres;

--
-- Name: suggestions_suggestion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.suggestions_suggestion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.suggestions_suggestion_id_seq OWNER TO postgres;

--
-- Name: suggestions_suggestion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.suggestions_suggestion_id_seq OWNED BY public.suggestions_suggestion.id;


--
-- Name: suggestions_suggestion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.suggestions_suggestion ALTER COLUMN id SET DEFAULT nextval('public.suggestions_suggestion_id_seq'::regclass);


--
-- Data for Name: suggestions_suggestion; Type: TABLE DATA; Schema: public; Owner: postgres
--

--
-- Name: suggestions_suggestion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.suggestions_suggestion_id_seq', 4, true);


--
-- Name: suggestions_suggestion suggestions_suggestion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.suggestions_suggestion
    ADD CONSTRAINT suggestions_suggestion_pkey PRIMARY KEY (id);


--
-- Name: suggestions_suggestion_discord_user_id_b75fbaee; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX suggestions_suggestion_discord_user_id_b75fbaee ON public.suggestions_suggestion USING btree (discord_user_id);


--
-- Name: suggestions_suggestion suggestions_suggesti_discord_user_id_b75fbaee_fk_discord_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.suggestions_suggestion
    ADD CONSTRAINT suggestions_suggesti_discord_user_id_b75fbaee_fk_discord_u FOREIGN KEY (discord_user_id) REFERENCES public.discord_user_discorduser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

