--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_managed_message_managedmessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_managed_message_managedmessage (
    id integer NOT NULL,
    channel_id bigint NOT NULL,
    message_id bigint,
    content text NOT NULL
);


ALTER TABLE public.discord_managed_message_managedmessage OWNER TO postgres;

--
-- Name: discord_managed_message_managedmessage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_managed_message_managedmessage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_managed_message_managedmessage_id_seq OWNER TO postgres;

--
-- Name: discord_managed_message_managedmessage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_managed_message_managedmessage_id_seq OWNED BY public.discord_managed_message_managedmessage.id;


--
-- Name: discord_managed_message_managedmessage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedmessage ALTER COLUMN id SET DEFAULT nextval('public.discord_managed_message_managedmessage_id_seq'::regclass);


--
-- Name: discord_managed_message_managedmessage discord_managed_message_managedmessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_managed_message_managedmessage
    ADD CONSTRAINT discord_managed_message_managedmessage_pkey PRIMARY KEY (id);


--
-- Name: discord_managed_message_managedmessage_channel_id_889475b7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_managed_message_managedmessage_channel_id_889475b7 ON public.discord_managed_message_managedmessage USING btree (channel_id);


--
-- Name: discord_managed_message_managedmessage_message_id_d17d8f97; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discord_managed_message_managedmessage_message_id_d17d8f97 ON public.discord_managed_message_managedmessage USING btree (message_id);


--
-- PostgreSQL database dump complete
--

