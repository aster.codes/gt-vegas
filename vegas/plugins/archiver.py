import asyncio
import datetime
import re
import time
from io import StringIO
from typing import Optional

import emoji
import markdown2
from blurple import io, ui
from discord import Emoji, File, Guild, Message, NotFound, PartialEmoji, TextChannel
from discord.ext import commands
from discord.ext.commands import Context, TextChannelConverter, has_any_role, is_owner
from jinja2 import Environment, FileSystemLoader

jinja_file_loader = FileSystemLoader("vegas/jinja2_templates")
jinja_env = Environment(loader=jinja_file_loader)


class Archiver(commands.Cog):
    def __init__(self, bot):
        """Startup for Archiver. Load jinja templates used for rendering HTML."""
        self.bot = bot
        self.lock = asyncio.Lock()
        self.base_template = jinja_env.get_template("channel-archive-base.html")
        self.style_base_template = jinja_env.get_template(
            "channel-archive-styles-base.html"
        )
        self.message_template = jinja_env.get_template("channel-archive-message.html")
        self.entry_template = jinja_env.get_template(
            "channel-archive-styles-entry.html"
        )
        self.video_template = jinja_env.get_template("channel-archive-video.html")

    @commands.group(
        hidden=True,
        case_insensitive=True,
        aliases=[
            "archve",
            "zip",
        ],
    )
    @is_owner()
    async def archive(self, context: Context):
        """Group command for archiving."""
        if context.invoked_subcommand is None:
            await context.send("Please use a subcommand.")

    def name_convert(self, name: str):
        """Helper function to remove disallowed characters"""

        return (
            name.lower()
            .replace(" ", "-")
            .replace("'", "-")
            .replace("/", "-")
            .replace(".", "-")
            .replace("!", "-")
            .replace("=", "-")
            .replace("(", "-")
            .replace(")", "-")
        )

    def role_styles(self, guild: Guild):
        """Helper function to create a CSS style block for each role in a channel"""
        entry_output = ""

        for role in guild.roles:
            if role.color.value != 0:
                rgb_value = f"rgb{role.color.to_rgb()}"
                role_name = self.name_convert(role.name)

                entry_output += self.entry_template.render(
                    role_name=role_name, rgb_value=rgb_value
                )
        full_styles = self.style_base_template.render(styles=entry_output)

        return full_styles

    def convert_embed(self, embed: dict) -> dict:
        """Converts an embeds data to a safe html version."""
        if not embed.get("type", False) in ["rich", "video", "link"]:
            return None
        if hasattr(embed, "color"):
            embed["color"] = "#" + hex(embed["color"])[2:]
        else:
            embed["color"] = "#000000"
        if embed.get("timestamp"):
            embed["timestamp"] = datetime.datetime.fromisoformat(
                embed["timestamp"]
            ).strftime("%b %d, %Y @ %I:%M %p")
        embed["description"] = markdown2.markdown(embed["description"])
        if hasattr(embed, "fields"):
            for count, field in enumerate(embed["fields"]):
                embed["fields"][count]["value"] = markdown2.markdown(field["value"])
        return embed

    async def sanatize_message_content(self, message: Message) -> str:
        """Helper function to sanatize a single discord.Message.content.
        Converts links, colors Role and User mentions. Saves and formats Image and Video attachments."""
        message_content = message.clean_content

        message_content = await self.render_video_embed(message, message_content)
        message_content = await self.render_user_and_role_mentions(
            message, message_content
        )
        message_content = await self.save_and_render_attachments(
            message, message_content
        )
        message_content = await self.render_custom_emoji(message, message_content)
        message_content = await self.render_unicode_emoji(message, message_content)

        return markdown2.markdown(message_content)

    async def render_video_embed(self, message: Message, message_content: str) -> str:
        """Helpder function to parse message.clean_content into a <video> element"""
        embed = dict()
        if message.embeds:
            embed = message.embeds[0].to_dict()
        if embed.get("type", False) in ["gifv", "link", "video"]:
            message_content = f"<a class='text-blue-400' href='{message.content}'>{message.content}</a>"
        if embed.get("type", False) == "gifv":
            message_content += self.video_template.render(embed=embed)
            return message_content
        if embed.get("type", False) in ["link", "video"]:
            return message_content
        return message_content

    async def render_user_and_role_mentions(
        self, message: Message, message_content: str
    ) -> str:
        """Helpder method to check for and color user and role mentions"""
        if message.mentions or message.role_mentions:
            for member in message.mentions:
                start = message_content.find("@" + member.name)
                end = start + len(member.name) + 1
                pre_mention = message_content[:start]
                mention = f"<span style='color:rgb{member.top_role.color.to_rgb()}'>@{member.name}</span>"
                post_mention = message_content[end:]
                message_content = f"{pre_mention}{mention}{post_mention}"
            for role in message.role_mentions:
                start = message_content.find("@" + role.name)
                end = start + len(role.name) + 1
                pre_mention = message_content[:start]
                mention = (
                    f"<span style='color:rgb{role.color.to_rgb()}'>@{role.name}</span>"
                )
                post_mention = message_content[end:]
                message_content = f"{pre_mention}{mention}{post_mention}"
        return message_content

    async def save_and_render_attachments(
        self, message: Message, message_content: str
    ) -> str:
        """Helper function to save and render all message.attachments.
        if a save_channel is not provided, vegas-logging will be used"""
        if message.attachments:
            await self.save_chan.send(
                f"Starting channel attachment archival\n"
                f"Category: `{message.channel.category.name}` - `{message.channel.category.id}`\n"
                f"Channel: `{message.channel.name}` - `{message.channel.id}`\n"
                f"Message: `{message.id}` sent by `{message.author.name}`"
            )
            for attachment in message.attachments:
                sf = await attachment.to_file()
                saved_attachment = await self.save_chan.send(file=sf)
                if any(ext in attachment.proxy_url for ext in ["png", "jpg", "gif"]):
                    nl = "\n"
                    message_content += (
                        f"{nl}{nl}![Image sent by {message.author.name}]"
                        f"({saved_attachment.attachments[0].proxy_url})"
                    )
                if any(ext in attachment.proxy_url for ext in ["mp4", "webm", "mov"]):
                    embed = {
                        "video": {
                            "url": saved_attachment.attachments[0].proxy_url,
                            "height": attachment.height,
                            "width": attachment.width,
                        }
                    }
                    message_content += self.video_template.render(embed=embed)
        return message_content

    async def render_custom_emoji(self, message: Message, message_content: str) -> str:
        """Helper function to check for and replace custom discord emojis with an image that will render in html"""
        custom_emojis = re.findall(r"<:\w*:\d*>", message_content)
        if custom_emojis:
            for cemoji in custom_emojis:
                start = message_content.find(cemoji)
                end = start + len(cemoji) + 1
                pre_emoji = message_content[:start]
                emoji_id = int(cemoji.split(":")[-1].split(">")[0])
                demoji = self.bot.get_emoji(emoji_id)
                try:
                    cemoji = f"<img class='lozad inline emoji' title=':{demoji.name}:' data-src='{demoji.url}' />"
                    post_emoji = message_content[end:]
                    message_content = f"{pre_emoji}{cemoji}{post_emoji}"
                except AttributeError:
                    pass
        return message_content

    async def render_unicode_emoji(self, message: Message, message_content: str) -> str:
        """Helper class to wrap emoji in a span and class to style"""
        emoji_list = emoji.emoji_lis(message_content)
        if emoji_list:
            for uemoji in emoji_list:
                start = message_content.find(uemoji["emoji"])
                end = start + len(uemoji["emoji"]) + 1
                pre_emoji = message_content[:start]
                emoji_name = emoji.demojize(uemoji["emoji"])
                uemoji = (
                    f"<span title='{emoji_name}' class='emoji'>{uemoji['emoji']}</span>"
                )
                post_emoji = message_content[end:]
                message_content = f"{pre_emoji}{uemoji}{post_emoji}"
        return message_content

    async def build_channel_html_doc(self):
        """Helper function to create an HTML document base"""
        output_doc = StringIO()
        styles = self.role_styles(self.ctx.guild)

        async for message in self.channel.history(limit=None, oldest_first=True):
            self.message_count += 1
            class_name = None
            message_content = None
            embed = None
            reactions = dict()
            try:
                await self.ctx.guild.fetch_ban(message.author)
            except NotFound:
                if hasattr(message.author, "top_role"):
                    class_name = self.name_convert(message.author.top_role.name)
                if message.embeds:
                    embed = message.embeds[0].to_dict()
                    embed = self.convert_embed(embed)
                if hasattr(message, "content"):
                    message_content = await self.sanatize_message_content(message)
                if hasattr(message, "reactions"):
                    for reaction in message.reactions:
                        emoji_id = (
                            reaction.emoji.id
                            if hasattr(reaction.emoji, "id")
                            else reaction.emoji
                        )
                        reactions[emoji_id] = dict()
                        reactions[emoji_id]["users"] = list()
                        reactions[emoji_id]["count"] = reaction.count
                        if reaction.custom_emoji:
                            print("Got Custom Emoji")
                            reactions[emoji_id]["custom_emoji"] = True
                            reactions[emoji_id]["name"] = reaction.emoji.name
                            reactions[emoji_id]["url"] = reaction.emoji.url
                        else:
                            reactions[emoji_id]["custom_emoji"] = False
                            reactions[emoji_id]["name"] = emoji.demojize(reaction.emoji)
                            reactions[emoji_id]["url"] = reaction.emoji
                        async for user in reaction.users():
                            reactions[emoji_id]["users"].append(user)

                output_doc.write(
                    self.message_template.render(
                        message=message,
                        message_content=message_content,
                        embed=embed,
                        reactions=reactions,
                        class_name=class_name,
                    )
                )
        output_doc.seek(0)
        html_doc = StringIO(
            self.base_template.render(
                channel=self.channel,
                timestamp=datetime.datetime.now(),
                styles=styles,
                bot_name=self.bot.user.name,
                message_count=self.message_count,
                body=output_doc.getvalue(),
            )
        )

        return html_doc

    @archive.command(
        case_insensitive=True,
    )
    @is_owner()
    async def channel(
        self,
        context: Context,
        channel: TextChannel,
        asset_channel: Optional[TextChannel] = None,
        output_channel: Optional[TextChannel] = None,
        dry_run: Optional[bool] = True,
    ):
        """Creates an HTML 'archive' of a channel

        Parameters
        ==========
        channel: TextChannel
            The channel to be archived. Accepts channel ID's, mentions or names.
        dry_run: Boolean (True/False)
            True is a 'test' run. False will truly archive and delete the channel.
        output_channel: Optional[TextChannel]
            The channel all output and the HTML archive will be sent to.
        asset_channel: Optional[TextChannel]
            The TextChannel for all Images, Files, Videos, etc to be saved to."""
        async with self.lock:
            self.message_count = 0
            self.channel = channel
            self.ctx = context
            self.dry_run = dry_run
            self.output_chan = output_channel
            if asset_channel:
                self.save_chan = asset_channel
            else:
                self.save_chan = await TextChannelConverter().convert(
                    ctx=self.ctx, argument="vegas-logging"
                )

            self.started = time.time()
            self.ended = None

            if self.dry_run:
                await self.ctx.send(
                    "Dry run! The channel archive document will be created, but the channel will not be touched."
                )

            if not self.dry_run:
                await self.lock_channel()
                await context.send(
                    f"Preparing to archive {channel.mention} {emoji.emojize(':locked:')}"
                )

            with self.ctx.typing():
                html_doc = await self.build_channel_html_doc()

            if self.output_chan:
                await self.output_chan.send(
                    content=f"Channel archive for {self.channel.name} ready for download!",
                    file=File(
                        fp=html_doc, filename=f"{self.channel.name}-archive.html"
                    ),
                )
            else:
                await self.ctx.send(
                    content=f"Channel archive for {self.channel.name} ready for download!",
                    file=File(
                        fp=html_doc, filename=f"{self.channel.name}-archive.html"
                    ),
                )

            self.ended = time.time()
            await context.send(f"Archival ran for {self.ended - self.started}")

            if not dry_run:
                await self.channel.delete(
                    reason=f"Deleted by {self.bot.user.name} on command by {self.ctx.message.author} channel archival"
                )

    async def lock_channel(
        self,
    ):
        """Helper function to lock the channel."""
        perms = self.channel.overwrites_for(self.ctx.guild.default_role)
        perms.send_messages = False
        await self.channel.set_permissions(self.ctx.guild.default_role, overwrite=perms)


def setup(bot):
    bot.add_cog(Archiver(bot))
