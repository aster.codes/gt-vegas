from discord.utils import get

from .base import BasePlugin


class RolesSubscriptionPlugin(BasePlugin):
    """
    on_ready is a global event that discordpy provides. You can check the
    docs under Events for more info.

    This method creates a subscription for the channel `roles-changes` on
    a local redis store. Redis must send a publish message with the
    following schema:

    PUBLISH roles-changes '<Server ID :Int>,<User ID :Int>, <Role Name :String>`

    If this schema is not used the bot will not be able to properly
    identify the role or member and the method will fail.

    """

    async def on_ready(self, context):
        async def roles_subscribe(bot):
            subscriber = await bot.redis.start_subscribe()
            await subscriber.subscribe(["roles-changes"])
            while True:
                reply = await subscriber.next_published()
                inputs = reply.value.split(",")
                server = self.bot.get_server(inputs[0])
                user = server.get_member(inputs[1])
                role = get(server.roles, name=inputs[2])
                await bot.remove_roles(user, role)
                print(
                    "Received: {} on channel {}".format(
                        repr(reply.value), reply.channel
                    )
                )
                print("Removed {} from {}".format(role, user))

        self.bot.loop.create_task(roles_subscribe(self.bot))


def setup(bot):
    import importlib

    if importlib.find_loader("asyncio_redis") is not None:
        bot.add_cog(RolesSubscriptionPlugin(bot))
    else:
        print(
            "Cannot load RolesSubscriptionPlugin, you dont seem to have",
            " asyncio_redis installed!",
        )
