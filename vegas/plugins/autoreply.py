"""This module provides the AutoReplyPlugin

Developed by Deborah#6709
Contributed: Asterisk*#0042
Contributed: NukeOfficial
"""
import datetime
import re

from discord.ext import commands

from .base import BasePlugin

QUESTION_REGEX = re.compile(r"question:(?=\s*\S+)")
PICREW_REGEX = re.compile(r".*(https:\/\/picrew\.me\/image_maker\/\d+).*")


class AutoReplyPlugin(BasePlugin):
    """This plugin manages vegas autoreplys and pinning."""

    @commands.Cog.listener(name="on_message")
    @commands.guild_only()
    async def picrew_channel_pinner(self, message):
        """Manages pinning picrews in the picrew channel"""

        if not message.guild:
            return

        if "picrew" in message.channel.name and message.author != self.bot.user:
            if re.match(PICREW_REGEX, message.content.lower()):
                await message.pin()

    @commands.Cog.listener(name="on_message")
    @commands.guild_only()
    async def picrew_channel_unpinner(self, message):
        """Manages unpinning picrews in the picrew channel"""

        if not message.guild:
            return

        if "picrew" in message.channel.name and message.author != self.bot.user:
            current_pins = await message.channel.pins()
            now = datetime.datetime.utcnow()
            sendable = False
            removed_message = "I removed some old picrews:\n\n"

            for pinned_message in current_pins:
                if (now - pinned_message.created_at).days > 7:
                    sendable = True
                    await pinned_message.unpin()
                    match = re.match(PICREW_REGEX, pinned_message.content.lower())
                    group = match.group(1)
                    removed_message += (
                        f"picrew: <{group}> - {pinned_message.jump_url}" + "\n"
                    )

            if sendable:
                await message.channel.send(removed_message, delete_after=30)

    @commands.Cog.listener(name="on_message")
    @commands.guild_only()
    async def question_vc_manager(self, message):
        """Manages pinning and unpinning questions in the VC channels"""

        if not message.guild:
            return

        if "mod-vc" in message.channel.name:
            if re.match(QUESTION_REGEX, message.content.lower()):
                await message.pin()

        elif "approved-vc" in message.channel.name:
            if re.match(QUESTION_REGEX, message.content.lower()):
                await message.pin()


def setup(bot):
    """Basic setup for AutoReplyPlugin"""
    bot.add_cog(AutoReplyPlugin(bot))
