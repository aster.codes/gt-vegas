"""
SuggestionsPlugin

"""
import asyncio

import discord
from checks import requires_postgres, setup_check_postgres
from discord.ext import commands
from discord.ext.commands import Context
from discord.utils import get
from settings.base import ACCEPT_EMOJI, CANCEL_EMOJI
from utils import (ReactionAddReplyDM, _check_suggestion_type,
                   _generate_suggestion_embed,
                   _interactive_get_other_submission,
                   _interactive_get_user_submission)

from plugins.base import BasePlugin

TIMEOUT = 120.0

REQUIRED_ROLE = "Loyal Theorist"
REVIEW_CHANNEL_NAME = "approved-feedback"
SUGGESTION_CHANNEL = "suggestions-and-community-support"
SUGGESTION_INSERT = "INSERT INTO suggestions_suggestion (suggestion, attachment, submitted, discord_user_id, approved) VALUES (%s, %s, %s, %s,  %s)"
SUGGESTION_SELECT = "SELECT * FROM suggestions_suggestion WHERE discord_user_id = %s ORDER BY submitted DESC"
SUGGESTION_GET = "SELECT * FROM suggestions_suggestion WHERE id = %s"
SUGGESTION_UPDATE = "UPDATE suggestions_suggestion SET approved='t' WHERE id = %s;"

OPEN_ROLES = ["Server Glue", "Approved Writer", "Approved Theorizer", "Approved Artist", "Approved Musician", "Expert Meme-er"]



class SuggestionPlugin(BasePlugin):
    """SuggestionsPlugin offers a way for guild members to submit suggestions
    to be reviewed by Mods, then voted on by everyone!"""

    def __init__(self, bot):
        self.open_roles = OPEN_ROLES
        self.bot = bot

    @commands.group(
        description="Have a suggestion?",
        case_insensitive=True,
        aliases=["suggestions", "suggest"],
    )
    @requires_postgres()
    async def suggestion(self, context: Context):
        """Base suggestion command for grouping"""

        if context.invoked_subcommand is None:
            await context.send("What do you want to do with Suggestions?")

    @suggestion.command(
        description="Add a suggestion for Staff Review!", aliases=["a", "+"]
    )
    @commands.dm_only()
    async def add(self, context: Context):
        """Allows a user to add a suggestion.
        Ensures the user is in the guild.
        Ensures the user has a high enough role.

        Uses
        ----
        - BasePlugin._get_guild()
        """
        guild = await self._get_guild(context)
        author = guild.get_member(context.message.author.id)
        link_id = await self._check_user_exists(author.id, guild.id)
        submission = ""
        submission_date = context.message.created_at.strftime("%b %d %y")
        required_role = get(guild.roles, name=REQUIRED_ROLE)
        attach_url = ""

        if not author.top_role.position >= required_role.position:
            await context.send(
                "You don't have a high enough role to add a suggestion. Please check out #server-info (https://discord.com/channels/353694095220408322/735635802960429087/735639337613656086) for more information."
            )

            return

        suggestion_type = await _check_suggestion_type(context)

        if suggestion_type == CANCEL_EMOJI:
            await context.send("Cya!")
            return

        if suggestion_type == "user":
            submission, attachments = await _interactive_get_user_submission(context, guild, self.open_roles)
        elif suggestion_type == "other":
            submission, attachments = await _interactive_get_other_submission(context)

        if not submission:
            return

        embed = await _generate_suggestion_embed(
            submission, author, guild, submission_date, attachments
        )

        review_message = await context.send(
            content=f"React with {ACCEPT_EMOJI} to send or {CANCEL_EMOJI} to cancel.", embed=embed
        )

        reaction = await ReactionAddReplyDM(context, validate=[ACCEPT_EMOJI, CANCEL_EMOJI], message=review_message).result()

        if str(reaction.emoji) == CANCEL_EMOJI:
            # TODO this might be broken?
            await review_message.edit(
                content="Sorry to hear that. If you have any suggestions send them to us!",
                embed=None,
            )

            return

        async with context.typing():
            await context.send("Trying to save your suggestion now...")
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    # Creates the new record
                    await cursor.execute(
                        SUGGESTION_INSERT,
                        (submission, attach_url, submission_date, link_id, False),
                    )
                    await cursor.execute(SUGGESTION_SELECT, (link_id,))
                    suggestions = await cursor.fetchall()
            embed.add_field(name="ID:", value=suggestions[0][0])
            af_channel = get(guild.text_channels, name=REVIEW_CHANNEL_NAME)
            af_message = await af_channel.send(embed=embed)
            await af_message.pin()
        await context.send(
            "Thanks for the suggestion! Your suggestion id is {}".format(
                suggestions[0][0]
            )
        )



    @add.error
    async def add_error(self, context: Context, error):
        """Error method for SuggestionsPlugin.add

        Checks for PrivateMessageOnly and asyncio.TimeoutError
        """

        if isinstance(error, commands.PrivateMessageOnly):
            await context.send(
                "You can only do that in a DM! The guild id you need is {}".format(
                    context.guild.id
                )
            )

        if isinstance(error, asyncio.TimeoutError):
            await context.send("Please react with 👍 or 👎", delete_after=5.0)

            return

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        """Watches for :OK: on a message in REVIEW_CHANNEL_NAME, by a Mod.

        Then moves Suggestion to #suggestions channel and updates DB"""

        if not payload.guild_id:
            return
        guild = self.bot.get_guild(payload.guild_id)
        channel = self.bot.get_channel(payload.channel_id)
        author = guild.get_member(payload.user_id)

        if str("🆗") != str(payload.emoji):
            return

        if isinstance(channel, discord.DMChannel):
            return

        if channel.name != REVIEW_CHANNEL_NAME:
            return

        if not any(role.name.lower() == "mods" for role in author.roles):
            await channel.send("Only mods can approve suggestions with 🆗!")

            return

        message = await channel.fetch_message(payload.message_id)
        embed = message.embeds[0]
        embed_dict = embed.to_dict()
        suggestion_id = int(embed_dict["fields"][2]["value"])

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(SUGGESTION_UPDATE, (suggestion_id,))
                await channel.send(
                    "{} approved suggestion #{}".format(author.mention, suggestion_id)
                )
        suggestion_channel = get(guild.text_channels, name=SUGGESTION_CHANNEL)
        embed.remove_field(2)
        await suggestion_channel.send(embed=embed)


def setup(bot):
    """Setup the Suggestions Plugin"""
    setup_check_postgres(bot, __file__)
    bot.add_cog(SuggestionPlugin(bot))
