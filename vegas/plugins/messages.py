from typing import Optional

from checks import requires_postgres, setup_check_postgres
from discord import Embed, Message, TextChannel
from discord.ext import commands
from discord.ext.commands import (BadArgument, ChannelNotFound, Context,
                                  MessageConverter, MessageNotFound,
                                  MissingRequiredArgument, has_any_role)
from discord.utils import get

from .base import BasePlugin


class MessagesPlugin(BasePlugin):
    @commands.group(
        description="Manage Messages",
        case_insensitive=True,
        aliases=[
            "mm",
        ],
    )
    @has_any_role("Mods", "Admin")
    async def managed_messages(self, context):
        if context.invoked_subcommand is None:
            await context.send("Please run a subcommand!")

    @managed_messages.command(
        name="update_managed_channel",
        description="Update channel messages that the bot manages",
        case_insensitive=True,
        aliases=[
            "umc",
        ],
    )
    @has_any_role("Mods", "Admin")
    @requires_postgres()
    async def update_managed_channel(self, context, channel_name):
        sql = "SELECT * FROM discord_managed_message_managedmessage AS mm LEFT JOIN discord_managed_message_managedembed AS me ON mm.id=me.messages_id WHERE channel_id = %s ORDER BY mm.id;"
        fields_sql = "SELECT * FROM discord_managed_message_managedembedfields WHERE embed_id = %s"
        mchannel = get(context.guild.text_channels, mention=channel_name)
        embed = None
        mmessage = None
        message = None

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql, [mchannel.id])
                messages_payload = await cur.fetchall()

                for mmessage in messages_payload:
                    if mmessage[5]:
                        embed_name = mmessage[5]
                        color = mmessage[6]
                        desc = mmessage[7]
                        image = mmessage[8]
                        thumbnail = mmessage[9]
                        footer = mmessage[10]
                        embed = Embed(
                            title=embed_name, color=int(color), description=desc
                        )
                        await cur.execute(fields_sql, [mmessage[4]])
                        embed_fields = await cur.fetchall()

                        if embed_fields:
                            for field in embed_fields:
                                print("Fields")
                                embed.add_field(
                                    name=field[1], value=field[2], inline=field[4]
                                )

                        if thumbnail:
                            embed.set_thumbnail(url=thumbnail)

                        if image:
                            embed.set_image(url=image)

                        if footer:
                            embed.set_footer(text=footer)

                    if mmessage[2] is not None:
                        message = await mchannel.fetch_message(mmessage[2])

                        if message.content != mmessage[3] or embed:
                            if embed:
                                await message.edit(content=mmessage[3], embed=embed)
                            else:
                                await message.edit(content=mmessage[3])
                            await context.send(
                                "Message ID: {} Updated".format(message.id)
                            )
                    else:
                        if embed:
                            message = await mchannel.send(
                                content=mmessage[3], embed=embed
                            )
                        else:
                            message = await mchannel.send(content=mmessage[3])
                        await cur.execute(
                            "UPDATE discord_managed_message_managedmessage SET message_id = %s WHERE id=%s;",
                            [message.id, mmessage[0]],
                        )
                        await context.send("Message ID: {} Sent!".format(message.id))
        await context.send("Complete!")

    @managed_messages.command(
        name="managed_scan_channel",
        description="Scan channel for bot messages to manage.",
        case_insensitive=True,
        aliases=[
            "msc",
        ],
    )
    @has_any_role("Mods", "Admin")
    @requires_postgres()
    async def managed_scan_channel(self, context, channel_name, limit=200):
        sql_insert = "INSERT INTO discord_managed_message_managedmessage (channel_id, message_id, content) VALUES (%s, %s, %s);"
        mchannel = get(context.guild.text_channels, mention=channel_name)
        message_count = 0
        async for message in mchannel.history(limit=limit):
            if (
                message.author.id == self.bot.user.id
            ):
                async with self.bot.pg_pool.acquire() as conn:
                    async with conn.cursor() as cur:
                        await cur.execute(
                            sql_insert, [mchannel.id, message.id, message.clean_content]
                        )
                        message_count += 1

        await context.send("{} messages logged to db!".format(message_count))

    @commands.command(
        description="Edit a message based on an id", case_insensitive=True
    )
    @has_any_role("Mods", "Admin")
    async def edit_message(
        self, context: Context, message: Message, *, message_content: str
    ):
        """Edits a message sent by this bot.

        Parameters
        ----------
        context: Context
            Default discord Context.
        old_message: Message
            The message to be edited.
        messaage: str
            The content to update to."""
        await message.edit(content=message_content)
        await context.send("Updated.")

    @commands.command(description="Send a message in a channel", case_insensitive=True)
    @has_any_role("Mods", "Admin")
    async def send_message(
        self,
        context: Context,
        channel: TextChannel,
        reply_message: Optional[str],
        *,
        message_content: str,
    ):
        """Sends a `message` in `channel`. Replies if `reply_message` is provided.

        Parameters
        ----------
        context: Context
            The default Discord Context
        channel: TextChannel
            The `channel` the message should be sent to.
        reply_message: Optional[str]
            The message to reply to. Can use message id.
        message: str
            The message you want to send."""

        true_reply_message: str = f"{channel.id}-{reply_message}"

        try:
            true_reply_message = await MessageConverter().convert(
                ctx=context, argument=true_reply_message
            )

            await channel.send(
                content=message_content,
                reference=true_reply_message,
                mention_author=False,
            )
        except MessageNotFound:
            message_content = f"{reply_message} {message_content}"
            await channel.send(content=message_content)
        await context.message.delete()

    async def cog_command_error(self, context, error):
        if isinstance(error, MissingRequiredArgument):
            arg = error.param.name

            if "channel" in arg:
                await context.send("Please provide a valid `channel`.")
            elif arg == "message":
                await context.send(
                    "Please provide a `message` to interact with. (usually an ID)"
                )
            elif arg == "message_content":
                await context.send("Please provide some `message_content` to use!")

        if isinstance(error, ChannelNotFound):
            await context.send("Please provide a valid `channel` to send messages to.")

    @commands.command(
        description="Makes the bot react to a message", case_insensitive=True
    )
    @has_any_role("Mods", "Admin")
    async def react(self, _, message: Message, emoji: str):
        """Causes vegas to react to a message with `emoji` found by `message_id`

        Parameters
        ----------
        message_id: int
            The ID of a `Message` to be lookedup.
        emoji: Emoji
            The Emoji to add as a reaction."""

        message = await message.add_reaction(emoji)
        await message.add_reaction("👍")

    @react.error
    async def react_error(self, context: Context, error):
        """Error handler for react"""

        if isinstance(error, MissingRequiredArgument):
            arg = error.param.name

            if "message_id" in arg:
                await context.send("Please provide a `Message ID`!")
            elif "emoji" in arg:
                await context.send("Please provide an emote to react with!")
            else:
                await context.send("¯\\_(ツ)_/¯")
        elif isinstance(error, BadArgument):
            await context.send(
                "Please provide a valid `Message ID` in the form of a number."
            )


def setup(bot):
    """Simple setup for requiring postgres"""
    setup_check_postgres(bot, __file__)
    bot.add_cog(MessagesPlugin(bot))
