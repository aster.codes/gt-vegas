"""This plugin provides access to the LampPlugin"""
import asyncio
from functools import partial
from typing import List, Optional, Union

from checks import requires_redis, setup_check_redis
from discord import (
    DMChannel,
    Embed,
    Guild,
    Member,
    Message,
    RawReactionActionEvent,
    Role,
    TextChannel,
    User,
)
from discord.ext import commands
from discord.ext.commands import (
    Context,
    MemberConverter,
    MissingRequiredArgument,
    Paginator,
    RoleConverter,
    TextChannelConverter,
    guild_only,
    has_any_role,
)
from discord.utils import get

from plugins.base import BasePlugin

LAMP_ROLE_NAME = "Lamp'd"
LAMP_CHANNEL_NAME = "the-lamp"
LAMP_EMBED_TITLE = "Current Lamp Info"
LAMP_QUEUE_NAME = "To Be Lamp'd"
LAMP_EVIDENCE_MESSAGE = "Evidence for "
LAMP_MESSAGE = f"You've been added to the channel {LAMP_CHANNEL_NAME}. This channel is a way that the Moderation Team can talk to you without other server members interupting, but still ensure  the conversation remains semi-open for all Mods to review.\n\n Someone will be with you momentarily."


class LampPlugin(BasePlugin):
    async def _lamp_channel(self, guild: Guild) -> TextChannel:
        return get(guild.text_channels, name=LAMP_CHANNEL_NAME)

    async def _lamp_role(self, guild: Guild) -> TextChannel:
        return get(guild.roles, name=LAMP_ROLE_NAME)

    async def _setup_lampd_members_field(self, embed: Embed, lamp_role: Role) -> Embed:
        field_pager = Paginator(prefix="", suffix="", max_size=256)
        lamp_members = lamp_role.members if lamp_role.members else "-"

        if lamp_members != "-":
            for member in lamp_role.members:
                field_pager.add_line(line=member.mention)

        if lamp_members != "-":
            for i, field in enumerate(field_pager.pages):
                embed.add_field(
                    name=f"Currently @{LAMP_ROLE_NAME} {i+1}", value=field, inline=False
                )
        else:
            embed.add_field(
                name=f"Currently @{LAMP_ROLE_NAME} 1", value=lamp_members, inline=False
            )

        return embed

    async def _setup_to_be_lampd_field(self, embed: Embed, guild: Guild) -> Embed:
        field_pager = Paginator(prefix="", suffix="", max_size=256)
        lamp_channel = await self._lamp_channel(guild)
        lamp_pins = await lamp_channel.pins()
        redis_queue_key = f"{guild.id}:lamp:queue_members"
        watching_users_ids = await self.bot.redis.smembers(
            redis_queue_key, encoding="utf-8"
        )

        if not watching_users_ids:
            embed.add_field(name=f"To Be @{LAMP_ROLE_NAME} 1", value="-", inline=False)

            return embed

        for user_id in watching_users_ids:
            user = get(guild.members, id=int(user_id))
            evidence_url = f"No evidence yet."

            for pin in lamp_pins:
                if f"{LAMP_EVIDENCE_MESSAGE} {user.id}" in pin.content:
                    evidence_url = f"[Message]({pin.jump_url})"

                    break
            field_pager.add_line(line=f"{user.mention} - {evidence_url}")

        for i, field in enumerate(field_pager.pages):
            embed.add_field(
                name=f"To Be @{LAMP_ROLE_NAME} {i+1}", value=field, inline=False
            )

        return embed

    async def _setup_lampd_watching_field(self, embed: Embed, guild: Guild) -> Embed:
        is_watching_key = f"{guild.id}:lamp:watching"
        is_watching = await self.bot.redis.exists(is_watching_key)
        watching_val = "🟢" if is_watching else "🔴"
        embed.add_field(name="Watch Mode:", value=watching_val)

        return embed

    async def _update_lamp_embed(self, guild: Guild) -> Embed:
        lamp_role: Role = await self._lamp_role(guild)
        lamp_channel: TextChannel = await self._lamp_channel(guild)
        description = f"**Current Lamp Message:** ```{LAMP_MESSAGE}```"
        embed = Embed(title=LAMP_EMBED_TITLE, description=description, color=000)
        embed.add_field(name="Current Lamp Channel:", value=f"{lamp_channel.mention}")
        embed.add_field(name="Current Lamp Role:", value=f"{lamp_role.mention}")
        embed = await self._setup_lampd_watching_field(embed, guild)
        embed = await self._setup_lampd_members_field(embed, lamp_role)
        embed = await self._setup_to_be_lampd_field(embed, guild)

        return embed

    async def _lamp_embed_message(
        self, guild: Guild, reply_channel: TextChannel, user: Member
    ) -> Message:
        lamp_channel = await self._lamp_channel(guild)
        pins = await lamp_channel.pins()

        for pin in pins:
            if hasattr(pin, "embeds") and len(pin.embeds) > 0:
                if pin.embeds[0].title == LAMP_EMBED_TITLE:
                    return pin

        await reply_channel.send(
            "No Lamp Embed Found... Would you like to set one now?"
        )
        try:
            while True:
                check_is_author_message = partial(
                    self._check_is_author_message_partial, user
                )
                response = await self.bot.wait_for(
                    "message", timeout=30, check=check_is_author_message
                )

                if response.content == "❌":

                    await reply_channel.send("No Lamp 4 u :(")

                    return
                elif response.content.lower() in [
                    "y",
                    "yes",
                    "ye",
                    "yeah",
                ]:
                    break
                else:
                    await reply_channel.send(
                        "I didn't understand that, try `yes` to continue or ❌ to exit."
                    )

        except asyncio.TimeoutError:
            await reply_channel.send("Your session timed out.")

        embed = await self._update_lamp_embed(guild)
        message = await lamp_channel.send(embed=embed)
        await message.pin()
        await reply_channel.send("Lamp tracking setup!")

        return message

    async def _update_queue(self, guild: Guild, watched_users: List[str]):
        await self.bot.redis.delete(f"{guild.id}:lamp:queue_members")

        for member_id in watched_users:

            if member_id != "-":
                member = get(guild.members, id=int(member_id))
                await self.bot.redis.sadd(f"{guild.id}:lamp:queue_members", member.id)

    @commands.group()
    @guild_only()
    @commands.has_any_role("Mods", "Disaster Director")
    async def lamp(self, context: Context):
        if context.invoked_subcommand is None:
            await context.send("Please use a subcommand")

    @lamp.command(
        aliases=[
            "t",
        ]
    )
    async def toggle_watching(self, context):
        """Togggles whether the bot will 'watch' for users on the lamp queue"""
        lamp_message: Message = await self._lamp_embed_message(
            context.guild, context.message.channel, context.message.author
        )
        is_watching_key = f"{context.guild.id}:lamp:watching"
        is_watching = await self.bot.redis.exists(is_watching_key)

        if is_watching:
            await self.bot.redis.delete(is_watching_key)
            message = "Bot lamp no longer watching"
        else:
            await self.bot.redis.set(is_watching_key, "watching")
            message = "Bot lamp now watching"
        lamp_embed: Embed = await self._update_lamp_embed(context.guild)
        await lamp_message.edit(embed=lamp_embed)
        await context.send(message)

    @lamp.command(
        aliases=[
            "r",
            "-",
        ]
    )
    async def remove(self, context: Context, member: Member):
        """Removes a user from the lamp queue.

        Parameter
        =========
        member: Member
            The member to be removed"""
        lamp_message: Message = await self._lamp_embed_message(
            context.guild, context.message.channel, context.message.author
        )
        watching_users_ids = await self.bot.redis.smembers(
            f"{context.guild.id}:lamp:queue_members", encoding="utf-8"
        )
        try:
            watching_users_set = set(watching_users_ids)
            watching_users_set.remove(str(member.id))
            await self._update_queue(context.guild, watching_users_set)
            lamp_embed: Embed = await self._update_lamp_embed(context.guild)
            await lamp_message.edit(embed=lamp_embed)
            out = f"User {member.mention} removed from Lamp queue!"
        except KeyError:
            out = f"User {member.mention} not on Lamp queue!"
        await context.send(out)

    async def _setup_evidence_for_user(self, message: Message, member: Member):
        if not hasattr(message, "attachments") or len(message.attachments) == 0:
            return
        lamp_channel = await self._lamp_channel(message.guild)
        lamp_pins = await lamp_channel.pins()
        evidence_message = None
        evidence_output = ""

        for pin in lamp_pins:
            # Convert old evidence/files

            if f"{LAMP_EVIDENCE_MESSAGE} {member.id}" in pin.content:
                evidence_message = pin

                evidence_output += (
                    pin.content.lstrip(
                        f"{LAMP_EVIDENCE_MESSAGE} {member.id} *a.k.a.* {member.mention} 1\n"
                    )
                    + "\n"
                )

                break

        for attachment in message.attachments:
            # Convert new evidence/files
            evidence_output += f"{attachment.proxy_url}\n"

        evidence_output += message.clean_content

        if evidence_message:
            await evidence_message.unpin()
            await evidence_message.delete()

        evidence_message = await lamp_channel.send(
            content=f"{LAMP_EVIDENCE_MESSAGE} {member.id} *a.k.a.* {member.mention} 1\n {evidence_output}"
        )

        await evidence_message.pin()

    @lamp.command(aliases=["ae", "+e"])
    async def add_evidence(self, context: Context, member: Member):
        """Add an image upload as evience against the provided Member.
        Requires an image upload.

        Parameter
        =========
        member: Member
            The member this image should be attached to"""
        lamp_message: Message = await self._lamp_embed_message(
            context.guild, context.message.channel, context.message.author
        )
        await self._setup_evidence_for_user(context.message, member)
        lamp_embed = await self._update_lamp_embed(context.guild)
        await lamp_message.edit(embed=lamp_embed)
        await context.send("Files Accepted!")

    @lamp.command(aliases=["a", "+"])
    async def add(self, context: Context, member: Member):
        """Adds a user to the lamp queue. (Opt. Upload a file for evidence).

        Parameters
        ==========
        member: Member
            The member to add to the queue"""
        lamp_message: Message = await self._lamp_embed_message(
            context.guild, context.message.channel, context.message.author
        )
        watching_users_ids = await self.bot.redis.smembers(
            f"{context.guild.id}:lamp:queue_members", encoding="utf-8"
        )
        watching_users_ids.append(member.id)
        await self._setup_evidence_for_user(context.message, member)
        await self._update_queue(context.guild, watching_users_ids)
        lamp_embed = await self._update_lamp_embed(context.guild)
        await lamp_message.edit(embed=lamp_embed)
        await context.send(
            content=f"Added {member.mention} to the list!", embed=lamp_embed
        )

    @lamp.command(
        aliases=[
            "l",
        ]
    )
    async def list(self, context: Context):
        """Displays an Embed with all of the Lamp settings"""
        lamp_message: Message = await self._lamp_embed_message(
            context.guild, context.message.channel, context.message.author
        )
        watching_users_ids = await self.bot.redis.smembers(
            f"{context.guild.id}:lamp:queue_members", encoding="utf-8"
        )
        await self._update_queue(context.guild, list(watching_users_ids))
        lamp_embed = await self._update_lamp_embed(context.guild)
        await lamp_message.edit(embed=lamp_embed)
        lamp_embed.add_field(
            name="Jump URL to Orginal:",
            value=f"[Original Lamp Message]({lamp_message.jump_url})",
            inline=False,
        )
        await context.send(embed=lamp_embed)

    async def _lamp_cleanup(self, member: Member):
        """Helper command to handle adding the lamp role to a user and updating embeds"""
        redis_queue_key = f"{member.guild.id}:lamp:queue_members"
        lamp_channel = await self._lamp_channel(member.guild)
        lamp_role = await self._lamp_role(member.guild)
        pins = await lamp_channel.pins()
        lamp_message = None

        await member.add_roles(lamp_role, reason="Automatically added by LampPlugin!")

        for pin in pins:
            if hasattr(pin, "embeds"):
                if pin.embeds[0].title == LAMP_EMBED_TITLE:
                    lamp_message = pin

                    break

        watching_users_ids = await self.bot.redis.smembers(
            redis_queue_key, encoding="utf-8"
        )

        watching_users_set = set(watching_users_ids)
        watching_users_set.discard(str(member.id))

        await self.bot.redis.delete(redis_queue_key)

        await self.bot.redis.sadd(redis_queue_key, *watching_users_set)

        lamp_embed = await self._update_lamp_embed(member.guild)
        await lamp_message.edit(embed=lamp_embed)

    @commands.Cog.listener(name="on_raw_reaction_add")
    async def lamp_reaction_watcher(self, payload: RawReactionActionEvent):
        """Handles on_raw_reaction_add if a user is in the lamp queue"""

        if not payload.guild_id:
            return

        guild = self.bot.get_guild(payload.guild_id)
        user = guild.get_member(payload.user_id)

        if user.bot:
            return

        lamp_role = await self._lamp_role(user.guild)

        if lamp_role.members:
            return

        if get(user.roles, name=LAMP_ROLE_NAME):
            return

        is_watching = await self.bot.redis.exists(f"{guild.id}:lamp:watching")

        if not is_watching:
            return

        watching_users_ids = await self.bot.redis.smembers(
            f"{guild.id}:lamp:queue_members", encoding="utf-8"
        )

        if str(payload.member.id) not in watching_users_ids:
            return

        await self._lamp_cleanup(user)

    @commands.Cog.listener(name="on_member_update")
    async def member_left_lamp(self, before: Member, after: Member):
        """Handles a member_update when a member leaves the LAMP_ROLE_NAME"""

        if get(after.roles, name=LAMP_ROLE_NAME):
            return

        if not any(
            LAMP_ROLE_NAME.lower() == after_role.name.lower()
            for after_role in after.roles
        ) and any(
            LAMP_ROLE_NAME.lower() == before_role.name.lower()
            for before_role in before.roles
        ):
            lamp_channel = await self._lamp_channel(after.guild)
            pins = await lamp_channel.pins()
            lamp_message = None

            for pin in pins:
                if hasattr(pin, "embeds"):
                    if pin.embeds[0].title == LAMP_EMBED_TITLE:
                        lamp_message = pin

                        break
            lamp_embed = await self._update_lamp_embed(after.guild)
            await lamp_message.edit(embed=lamp_embed)

    @commands.Cog.listener(name="on_member_update")
    async def lamp_member_update_watcher(self, before: Member, after: Member):
        """Handles a member_update if the user is on the lamp queue"""

        if get(after.roles, name=LAMP_ROLE_NAME):
            return
        lamp_role = await self._lamp_role(after.guild)

        if lamp_role.members:
            return

        is_watching = await self.bot.redis.exists(f"{after.guild.id}:lamp:watching")

        if not is_watching:
            return
        watching_users_ids = await self.bot.redis.smembers(
            f"{after.guild.id}:lamp:queue_members", encoding="utf-8"
        )

        if str(after.id) in watching_users_ids:
            await self._lamp_cleanup(before)

    @commands.Cog.listener(name="on_member_update")
    async def lamp_role_added(self, before: Member, after: Member):
        """Handles the automatic bot messaging when a member is added to the Lamp channel.
        Direct Messaged are made from `LAMP_MESSAGE`.
        The bot also sends `LAMP_MESSAGE` to `LAMP_CHANNEL_NAME` and pings `after.mention`

        Parameters
        ----------
        before: Member
            The Member "before" the update.
        after: Member
            The Member "after" the update."""

        lamp_role = await self._lamp_role(after.guild)

        if not lamp_role.members:
            return

        if any(
            LAMP_ROLE_NAME.lower() == after_role.name.lower()
            for after_role in after.roles
        ) and not any(
            LAMP_ROLE_NAME.lower() == before_role.name.lower()
            for before_role in before.roles
        ):
            lamp_chan = get(before.guild.text_channels, name=LAMP_CHANNEL_NAME)
            await before.send(f"Hello from {before.guild.name}!\n\n" + LAMP_MESSAGE)
            await lamp_chan.send(f"{before.mention}\n\n" + LAMP_MESSAGE)
            await self._lamp_cleanup(before)

    @commands.Cog.listener(name="on_message")
    async def lamp_message_watcher(self, message: Message):
        """Handles a on_message if the user is on the lamp queue"""

        if isinstance(message.channel, DMChannel):
            return

        lamp_role = await self._lamp_role(message.guild)

        if hasattr(lamp_role, "members"):
            return

        if message.author.bot:
            return

        if get(message.author.roles, name=LAMP_ROLE_NAME):
            return

        is_watching = await self.bot.redis.exists(f"{message.guild.id}:lamp:watching")

        if not is_watching:
            return

        watching_users_ids = await self.bot.redis.smembers(
            f"{message.guild.id}:lamp:queue_members", encoding="utf-8"
        )

        if str(message.author.id) in watching_users_ids:
            await self._lamp_cleanup(message.author)

    @commands.Cog.listener(name="on_typing")
    async def lamp_typing_watcher(
        self, channel: TextChannel, user: Union[Member, User], when
    ):
        """Handles a on_typing if the user is on the lamp queue"""

        if isinstance(channel, DMChannel):
            return
        lamp_role = get(user.guild.roles, name=LAMP_ROLE_NAME)

        if hasattr(lamp_role, "members"):
            return

        if user.bot:
            return

        if isinstance(user, User):
            return

        if get(user.roles, name=LAMP_ROLE_NAME):
            return

        is_watching = await self.bot.redis.exists(f"{user.guild.id}:lamp:watching")

        if not is_watching:
            return

        watching_users_ids = await self.bot.redis.smembers(
            f"{channel.guild.id}:lamp:queue_members", encoding="utf-8"
        )

        if str(user.id) not in watching_users_ids:
            return

        await self._lamp_cleanup(user)


def setup(bot):
    """Simple setup for LampPlugin"""
    setup_check_redis(bot, __file__)
    bot.add_cog(LampPlugin(bot))
