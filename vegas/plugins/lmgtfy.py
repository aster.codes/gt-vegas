from discord.ext import commands

from .base import BasePlugin


# All plugins should inherit form BasePlugin
class LMGTFYPlugin(BasePlugin):
    # If you want to use sub commands you must use this decorator to create a group
    @commands.command(description="Ever wonder how something works?")
    async def google(self, context, *google):
        if google:
            if "@everyone" in google:
                await context.send(
                    "You're going to get in trouble using everyone like that"
                )
            elif "@here" in google:
                await context.send(
                    "You're going to get in trouble using here like that"
                )
            else:
                google = "+".join(google)
                await context.send(
                    "I am not your fucking Google Assistant. If you are on Discord, you have access to the internet. Google it yourself."
                )
                await context.send("https://lmgtfy.com/?q={}".format(google))

        else:
            await context.send("Hey genius, I can't google nothing.")


def setup(bot):
    bot.add_cog(LMGTFYPlugin(bot))
