import datetime
from typing import Optional, Tuple

import emoji
from blurple import io, ui
from checks import is_voting_guild, requires_postgres, setup_check_postgres
from discord import Attachment, Embed, Guild, Member, TextChannel
from discord.abc import GuildChannel
from discord.ext import commands
from discord.ext.commands import (
    CategoryChannelConverter,
    Cog,
    Context,
    MessageConverter,
    MissingRequiredArgument,
    TextChannelConverter,
    has_role,
)
from discord.utils import get
from exceptions.suggestions import MissingEmbedError, MissingPinsError
from settings.base import (
    ACCEPT_EMOJI,
    APPROVED_ROLES,
    CANCEL_EMOJI,
    LB,
    OK_HAND,
    VOTING_GUILD_ID,
)
from utils import (
    ReactionAddReplyDM,
    _check_suggestion_type,
    _generate_suggestion_embed,
    _interactive_get_other_submission,
    _interactive_get_user_submission,
    make_guild_embed,
)

from plugins.base import BasePlugin

TITLE_EMOJI = emoji.emojize(":red_exclamation_mark:")
DESC_EMOJI = emoji.emojize(":speech_balloon:")
ADD_FIELD_EMOJI = emoji.emojize(":plus:")
REMOVE_FIELD_EMOJI = emoji.emojize(":minus:")
STOPWATCH_EMOJI = "⏱️"

SUGGESTION_EDIT_MENU = (
    f"{TITLE_EMOJI} - Change Title{LB}"
    f"{DESC_EMOJI} - Change Description{LB}"
    f"{ADD_FIELD_EMOJI} - Add Comment{LB}"
    f"{REMOVE_FIELD_EMOJI} - Remove Comment{LB}"
    f"{OK_HAND} - Save and Exit"
)

AP_VOTING_INSTRUCTIONS = (
    "Locked and sent to voting on {timestamp}"
    f"{LB}"
    f"Please take this time to vote!{LB}{LB}"
    f"Vote by reacting with one of the following:{LB}"
    f"- {ACCEPT_EMOJI} to vote in support{LB}"
    f"- {CANCEL_EMOJI} to vote against{LB}"
    f"- {STOPWATCH_EMOJI} to defer to later"
)


class ApprovedVotingPlugin(BasePlugin):
    @requires_postgres()
    @commands.group(
        description="Have a suggestion?",
        case_insensitive=True,
        aliases=[
            "sv",
        ],
    )
    async def suggestion_voting(self, context):
        """Base suggestion command for grouping"""

        if context.invoked_subcommand is None:
            await context.send("What do you want to do with Suggestions Voting?")

    @suggestion_voting.command(case_insensitive=True, aliases=["new", "n", "+"])
    @commands.has_any_role(*APPROVED_ROLES)
    @is_voting_guild()
    async def new_suggestion(self, context: Context):
        """
        Allows any approved member to submmit a new suggestion.
        """
        discussion_category = get(context.guild.categories, name="Discord Discussion")
        today = datetime.datetime.now().strftime("%m\/%d\/%Y %I:%M %p %Z")

        await context.send("What kind of suggestion would you like to make?")
        await context.send(
            f"(Reply with `Event` to start an event suggestion or `Other` to suggest something else. {CANCEL_EMOJI} exits the suggestion.)"
        )
        type_message = await io.MessageReply(
            context, validate=["Other", "Event", CANCEL_EMOJI]
        ).result()

        if type_message.content == CANCEL_EMOJI:
            await context.send("Cya!")
            return

        elif type_message.content.lower() == "other":
            submission, attachments = await _interactive_get_other_submission(context)
            if not submission:
                return
            embed = await _generate_suggestion_embed(
                submission, context.message.author, context.guild, today, attachments
            )
        elif type_message.content.lower() == "event":
            submission, embed_fields = await _interactive_get_event(context)
            if not submission:
                return
            embed = Embed(title="PlaceholderName", description=submission)
            embed.add_field(
                name="Event Owners:", value=embed_fields["owner"], inline=False
            )
            embed.add_field(
                name="Event Channels:", value=embed_fields["channels"], inline=False
            )
            embed.set_author(
                name=context.author.name + "#" + context.author.discriminator,
                icon_url=context.author.avatar_url,
            )
            embed.set_thumbnail(url=context.guild.icon_url)
            embed.add_field(name="Author:", value=context.author.mention, inline=True)
            embed.add_field(name="Submission Date:", value=today, inline=True)

        await context.send("What do you want the title of this suggestion to be?")
        title_message = await io.MessageReply(context).result()
        embed.title = title_message.content

        review_message = await context.send(
            content=f"React with {ACCEPT_EMOJI} to send or {CANCEL_EMOJI} to cancel.",
            embed=embed,
        )

        reaction = await ReactionAddReplyDM(
            context, validate=[ACCEPT_EMOJI, CANCEL_EMOJI], message=review_message
        ).result()

        if str(reaction.emoji) == CANCEL_EMOJI:
            await review_message.edit(
                content="Sorry to hear that. If you have any suggestions send them to us!",
                embed=None,
            )

            return
        async with context.typing():
            new_suggestion_channel = await context.guild.create_text_channel(
                f"{embed.title}", category=discussion_category, slowmode_delay=1200
            )
            new_suggestion_message = await new_suggestion_channel.send(embed=embed)
            await new_suggestion_message.pin()
            await ui.Alert(
                ui.Style.SUCCESS, title="Success", description=f"Submitted!"
            ).send(context)

    @suggestion_voting.command(
        case_insensitive=True,
        aliases=[
            "mtd",
        ],
    )
    @commands.has_any_role("Mods")
    async def move_to_discussion(
        self, context: Context, voting_guild: int, suggestion_id: int
    ):
        """
        Moves a suggestion embed to a voting guild and creates a channel for discussion.

        Parameters
        ----------
        voting_guild: int
            The id of a discord.Guild.
        suggestion_id: int
            The id of the discord.Message with the suggestion embed.
        Todo
        ----
            - validate embed
        """
        suggestion_chan = await TextChannelConverter().convert(
            ctx=context, argument="suggestions-and-community-support"
        )
        message = await MessageConverter().convert(
            ctx=context, argument=f"{suggestion_chan.id}-{suggestion_id}"
        )
        guild = self.bot.get_guild(voting_guild)
        suggestion_embed = message.embeds[0]
        if not guild:
            await context.send("I am not in a guild with the ID: `{voting_guild}`")
            return
        discussion_category = get(guild.categories, name="Discord Discussion")

        if not message.embeds:
            raise MissingEmbedError(message=message)

        guild_embed = make_guild_embed(guild)

        await context.send(
            content="I have selected the following guild:", embed=guild_embed
        )

        await context.send(
            "Suggestions from suggestion channels are all automatically labeled `New Suggestion!`"
            " and the channel name will be #new-suggestion. Please provide a new name."
        )
        new_title = await io.MessageReply(context).result()

        suggestion_embed.title = new_title.content
        reply_message = await context.send(
            content="Is this correct?", embed=suggestion_embed
        )
        reply = await io.ReactionAddReply(
            context, validate=[ACCEPT_EMOJI, CANCEL_EMOJI], message=reply_message
        ).result()

        if str(reply.emoji) == CANCEL_EMOJI:
            await ui.Alert(ui.Style.WARNING, f"Okay! Cancelling!").send(context)
            return

        new_suggestion_channel = await guild.create_text_channel(
            f"{suggestion_embed.title}",
            category=discussion_category,
            slowmode_delay=1200,
        )
        new_suggestion_message = await new_suggestion_channel.send(
            embed=suggestion_embed
        )
        await new_suggestion_message.pin()
        await ui.Alert(
            ui.Style.SUCCESS,
            title="Success",
            description=f"Moving suggestion [{message.id}]({message.jump_url}) to {guild.name} as [{new_suggestion_channel.name}]({new_suggestion_message.jump_url})",
        ).send(context)

    @suggestion_voting.command(
        case_insensitive=True, aliases=["u", "update-suggestion"]
    )
    @commands.has_any_role(*APPROVED_ROLES)
    @is_voting_guild()
    async def update_suggestion(self, context: Context):
        """
        Update a suggestion. (Must be in the suggestion channel.)
        """
        pins = await context.channel.pins()

        if len(pins) > 49:
            pass

        if not pins:
            raise MissingPinsError(channel=context.channel)
        if not pins[0].embeds:
            raise MissingEmbedError(message=pins[0])

        suggestion_dict = pins[0].embeds[0].to_dict()
        suggestion_message = await context.send(
            content=SUGGESTION_EDIT_MENU, embed=pins[0].embeds[0]
        )

        while True:
            suggestion_edit = Embed().from_dict(suggestion_dict)
            await suggestion_message.edit(
                content=SUGGESTION_EDIT_MENU, embed=suggestion_edit
            )
            reply = await io.ReactionAddReply(
                context,
                validate=[
                    TITLE_EMOJI,
                    DESC_EMOJI,
                    ADD_FIELD_EMOJI,
                    REMOVE_FIELD_EMOJI,
                    OK_HAND,
                    CANCEL_EMOJI,
                ],
                message=suggestion_message,
            ).result()

            if str(reply.emoji) == TITLE_EMOJI:
                title_message = await context.send(
                    "What do you want to set the new suggestion title to?"
                )
                new_title = await io.MessageReply(context).result()
                suggestion_dict["title"] = f"Suggestion: {new_title.content}"
                await context.channel.edit(name=new_title.content)
                await title_message.delete()

            elif str(reply.emoji) == DESC_EMOJI:
                desc_message = await context.send(
                    "What do you want to set the new suggestion description to?"
                )
                new_desc = await io.MessageReply(context).result()
                suggestion_dict["description"] = new_desc.clean_content
                await desc_message.delete()

            elif str(reply.emoji) == ADD_FIELD_EMOJI:
                embed_fields = suggestion_dict["fields"]

                if len(embed_fields) == 25:
                    first_update_idx = [
                        field_idx
                        for field_idx, field in enumerate(embed_fields)
                        if "Update" in field["name"]
                    ][0]
                    del suggestion_dict["fields"][int(first_update_idx)]

                update_message = await context.send(
                    "What comment/update do you want to add? (Max 1024)"
                )
                new_update = await io.MessageReply(
                    context, validate=r".{1,1024}"
                ).result()
                timestamp = datetime.datetime.now().strftime("%m\/%d\/%Y %I:%M %p %Z")
                author = f"{context.author.name}#{context.author.discriminator}"
                suggestion_dict["fields"].append(
                    {
                        "name": f"Update: @{timestamp} - {author}",
                        "value": new_update.clean_content,
                        "inline": False,
                    }
                )
                await update_message.delete()

            elif str(reply.emoji) == REMOVE_FIELD_EMOJI:
                update_fields = [
                    (field_idx, field["name"])
                    for field_idx, field in enumerate(embed_fields)
                    if "Update" in field["name"]
                ]
                menu = f"**Which field would you like to remove? (Reply wiht a number.**){LB}"
                idxs = list()
                for field in update_fields:
                    idxs.append(str(field[0]))
                    menu += f"{field[0]} - {field[1]}{LB}"
                menu_message = await context.send(menu)
                remove_message = await io.MessageReply(context, validate=idxs).result()
                del suggestion_dict["fields"][int(remove_message.content)]
                await menu_message.delete()

            elif str(reply.emoji) == OK_HAND:
                suggestion_edit = Embed().from_dict(suggestion_dict)
                update = await context.send(embed=suggestion_edit)
                await update.pin()
                break

            elif str(reply.emoji) == CANCEL_EMOJI:
                await context.send("Cya!")
                break
        await suggestion_message.delete()

    @suggestion_voting.command(case_insensitive=True, aliases=["mtv", "move-to-voting"])
    @commands.has_any_role("Mods")
    @is_voting_guild()
    async def move_to_voting(self, context: Context, suggestion_channel: TextChannel):
        """
        Moves a given `suggestion_channel` to the Voting category.

        Syncs permissions with the category.

        Parameters
        ==========
        suggestion_channel: TextChannel
            The Suggestion channel to be moved.
        """
        voting_category = await CategoryChannelConverter().convert(
            ctx=context, argument="Voting"
        )
        await suggestion_channel.edit(
            position=0, category=voting_category, sync_permissions=True
        )

    @suggestion_voting.command(case_insensitive=True)
    @commands.has_role("Mods")
    @is_voting_guild()
    async def accept(self, context: Context, suggestion_channel: TextChannel):
        """
        Accepts a suggestion and moves the channel to the proper category.

        Paramters
        =========
            suggestion_channel: TextChannel
                The suggestion to accept.
        """
        voting_category = await CategoryChannelConverter().convert(
            ctx=context, argument="Accepted and Running"
        )
        await suggestion_channel.edit(
            position=0, category=voting_category, sync_permissions=True
        )

    @suggestion_voting.command(case_insensitive=True)
    @commands.has_role("Mods")
    @is_voting_guild()
    async def complete(self, context: Context, suggestion_channel: TextChannel):
        """
        Completes an accepted suggestion and archives the channel.

        Parameters
        ==========
            suggestion_channel: TextChannel
                The suggestion/channel to be accepted.
        """
        now = datetime.datetime.now().strftime("%x @ %-I:%M %p")
        archive_cmd = self.bot.get_command("archive_channel")
        accept_chan = await TextChannelConverter().convert(
            ctx=context, argument="accepted-archived"
        )

        await accept_chan.send(
            f"#{suggestion_channel.name} was accepted and completed! Archived on {now} by {context.author.mention}"
        )
        await context.invoke(archive_cmd, suggestion_channel, False, accept_chan)

    @suggestion_voting.command(case_insensitive=True)
    @commands.has_role("Mods")
    @is_voting_guild()
    async def reject(self, context: Context, suggestion_channel: TextChannel):
        """
        Rejcts a suggestion.

        Parameters
        ==========
            suggestion_channel: TextChannel
                The suggestion/channel to be rejected.
        """
        now = datetime.datetime.now().strftime("%x @ %-I:%M %p")
        archive_cmd = self.bot.get_command("archive_channel")
        rejected_chan = await TextChannelConverter().convert(
            ctx=context, argument="rejected-archived"
        )

        await rejected_chan.send(
            f"#{suggestion_channel.name} was rejected. Archived on {now} by {context.author.mention}"
        )
        await context.invoke(archive_cmd, suggestion_channel, False, rejected_chan)

    @commands.Cog.listener(name="on_guild_channel_update")
    async def voting_ensure_sync(self, before: GuildChannel, after: GuildChannel):
        """
        A listener that ensures any TextChannel in the "Voting" category has its permissions sync'd.
        """
        delay = 1200
        if after.category.name.lower() == "voting":
            delay = 0
        if after.category.name.lower() in [
            "voting",
            "discord discussion",
            "subreddit discussion",
        ]:
            if after.category.name.lower() != before.category.name.lower():
                await after.edit(sync_permissions=True, slowmode_delay=delay)

    @commands.Cog.listener(name="on_guild_channel_update")
    async def ensure_pins_limit(self, before: GuildChannel, after: GuildChannel):
        """
        A listener that ensures any TextChannel in the "Voting" category has less than 50 pins.
        """
        if after.category.name.lower() == "voting":
            channel_pins = await after.pins()

            if len(channel_pins) > 49:
                await channel_pins[50].unpin()

    @commands.Cog.listener(name="on_guild_channel_update")
    async def discussion_voting_ensure_pins_and_embed(
        self, before: GuildChannel, after: GuildChannel
    ):
        """
        A listener that ensures the latest pin in Voting and Discussion categories has an embed.
        """
        if after.category.name.lower() in [
            "voting",
            "discord discussion",
            "subreddit discussion",
        ]:
            channel_pins = await after.pins()
            if not channel_pins[0].embeds:
                await channel_pins[0].unpin()

    @commands.Cog.listener(name="on_guild_channel_update")
    async def voting_add_instructions(self, before: GuildChannel, after: GuildChannel):
        """
        Ensures that Channels moving into the Voting category get a summary and voting instructions.
        """
        if (
            after.category.name.lower() == "voting"
            and before.category.name.lower() != "voting"
        ):
            channel_pins = await after.pins()
            if "locked" not in channel_pins[0].content.lower():
                timestamp = datetime.datetime.now().strftime("%m\/%d\/%Y %I:%M %p %Z")
                vote_message = await after.send(
                    content=AP_VOTING_INSTRUCTIONS.format(timestamp=timestamp),
                    embed=channel_pins[0].embeds[0],
                )
                await vote_message.add_reaction(ACCEPT_EMOJI)
                await vote_message.add_reaction(CANCEL_EMOJI)
                await vote_message.add_reaction("⏱️")
                await vote_message.pin()

    @suggestion_voting.command(case_insensitive=True)
    @commands.has_role("Mods")
    @is_voting_guild()
    async def sync_member(self, context: Context, voting_member: Member):
        """
        Syncs a Members roles from GTD to Voting server.

        Parameters
        ==========
            voting_member: Member
                The meber in the voting server to sync.
        """
        gtd_guild = self.bot.get_guild(353694095220408322)
        voting_guild = self.bot.get_guild(VOTING_GUILD_ID)
        await _sync_member(voting_member, voting_guild, gtd_guild)

    @Cog.listener(name="on_member_join")
    async def sync_user_roles_on_join(self, voting_member: Member):
        """
        Bot Listener that syncs a Members roles from GTD to Voting server.
        """
        if voting_member.guild.id == VOTING_GUILD_ID:
            gtd_guild = self.bot.get_guild(353694095220408322)
            voting_guild = self.bot.get_guild(VOTING_GUILD_ID)
            await _sync_member(voting_member, voting_guild, gtd_guild)

    async def cog_command_error(self, context: Context, error: Exception):
        if isinstance(error, (MissingPinsError)):
            await context.send(
                f"It looks like {error.channel.mention} doesn't have any pins right now."
            )
        if isinstance(error, (MissingEmbedError)):
            await context.send(
                "It looks like the last pin in this channel does not have an embed. That is required for this command to work."
            )
        print(error)


def setup(bot):
    """Setup the Suggestions Plugin"""
    setup_check_postgres(bot, __file__)
    bot.add_cog(ApprovedVotingPlugin(bot))


async def _sync_member(voting_member: Member, voting_guild: Guild, gtd_guild: Guild):
    """
    Syncs a Members roles from GTD to Voting server.

    Parameters
    ==========
        voting_member: Member
            The meber in the voting server to sync.
    """
    welcome_chan = get(voting_guild.text_channels, name="general")
    server_rules = get(voting_guild.text_channels, name="server-rules")
    how_to_participate = get(voting_guild.text_channels, name="how-to-participate")
    gtd_member = get(gtd_guild.members, id=voting_member.id)
    needed_role_names = [
        gtd_member_role.name
        for gtd_member_role in gtd_member.roles
        if gtd_member_role.name in APPROVED_ROLES
    ]
    needed_roles = list()
    for role_name in needed_role_names:
        needed_role = get(voting_guild.roles, name=role_name)
        needed_roles.append(needed_role)
    await voting_member.add_roles(*needed_roles, reason="Joined Voting Server")
    welcome_format = (
        f"Welcome to GTD Voting {voting_member.mention}!{LB}{LB}"
        f"Make sure you understand that {server_rules.mention} apply. "
        f"You should also read {how_to_participate.mention} and learn a little about how this server works! "
        f"Lastly if you have any questions, just ask in {welcome_chan.mention}! We want to make sure everyone is comfortable with this change!{LB}{LB}"
        f"We also added the following roles to you!{LB}"
    )

    welcome_message = await welcome_chan.send(welcome_format)
    welcome_format += "\n".join(needed_role.mention for needed_role in needed_roles)
    await welcome_message.edit(content=welcome_format)


async def _interactive_get_event(context: Context) -> Tuple[str, dict]:
    await context.send("Okay! Let's start some event planning!")
    await context.send("**Tell us about the event.**")
    await context.send(
        "[Give a detailed description of what this event is and why the server should host the event. You should include information such as who would be interested in participating and how many people you expect to be included.]"
    )
    await context.send("[Accepts: between 80 - 1500 characters]")
    event_desc = await io.MessageReply(context, validate=r"^(.{80,1500}|❌)$").result()

    if event_desc.content.lower() == CANCEL_EMOJI:
        await context.send("Cya!")

        return

    await context.send("**Who will be running this event?**")
    await context.send(
        "[This should include all users who will be in charge of or running the event. For best chances of success having an Approved or Mod will also help.]"
    )
    await context.send("[Accepts: between 1 - 1024 characters]")
    event_owners = await io.MessageReply(context, validate=r"^(.{1,1024}|❌)$").result()

    if event_owners.content.lower() == CANCEL_EMOJI:
        await context.send("Cya!")

        return

    await context.send("**What channels will this even be hosted in?**")
    await context.send("[List channels here.]")
    await context.send("[Accepts: between 1 - 1024 characters]")
    event_channels = await io.MessageReply(
        context, validate=r"^(.{1,1024}|❌)$"
    ).result()

    if event_channels.content.lower() == CANCEL_EMOJI:
        await context.send("Cya!")

        return

    await context.send("**Please provide a timeline of the event.**")
    await context.send(
        "[List dates as either a bulleted list or date ranges:\n"
        "* Dec 12 - Event Start, instructions posted in comments below are reposted in #writing, users can get role now\n"
        "* Dec 13 - Dec 15 - End of Phase 1, winners awarded XP\n"
        "* Dec 16 - Start of Phase 2, ...]"
    )
    await context.send(
        "[Accepts: between 1 - 2550 characters, if you require more space, use a google doc.]"
    )

    def timeline_validation(message):
        return len(message.content) > 1 and len(message.content) < 2550

    event_timeline = await io.MessageReply(
        context, validate=timeline_validation
    ).result()

    if event_timeline.content.lower() == CANCEL_EMOJI:
        await context.send("Cya!")

        return

    return (
        f"**Event Details:**{LB}{event_desc.content}{LB}**Event Timeline:**{LB}{event_timeline.content}",
        {"owner": event_owners.content, "channels": event_channels.content},
    )
