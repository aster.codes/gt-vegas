"""This Cog provides the EmbedPlugin plugin form Approved members to interactively build, edit and post embeds

DEFAULT_TIMEOUT: float
    The default time for embeds to wait before timing out.
ROLES: List[str]
    A list of partial roles the EmbedPlugin is allowed to ping
"""
import asyncio
from functools import partial

import discord
from blurple.io import MessageReply
from discord import Message, Role
from discord.ext import commands
from discord.ext.commands import (
    BadArgument,
    Context,
    RoleConverter,
    TextChannelConverter,
    has_any_role,
)

from .base import BasePlugin

DEFAULT_TIMEOUT = 380.0

ROLES = [
    "approved",
    "mods",
    "critic",
    "expert",
    "glue",
    "notif",
    "no",
    "deadlock",
    "patchwork collective",
]

EMBED_MENU = (
    "💬 - Change Description\n"
    "🖼️ - Change Icon\n"
    "📦 - Add Server Logo\n"
    "🎨 - Change Image\n"
    "👣 - Change Footer\n"
    "➕ - Add Field\n"
    "🖊️  Change Field\n"
    "💢 - Remove Field\n"
    "🌈 - Change Color\n"
    "📰 - Change text on Publish\n"
    "📌 - Pin Embed on Publish\n"
    "🏓 - Add Ping(s) to Publish\n"
    "❓ - Show Ping/Pin Status\n"
    "🆗 - Publish to Channel\n"
    "❌ - Cancel\n"
)


async def fallback(*_):
    """Fallback method so the embed dosnt crash on an unknown emoji"""
    print("Fallback in case theres no correct input")


class EmbedPlugin(BasePlugin):
    """This COG provides an interactive way for Approved members to
    build, edit and post embeds."""

    def __init__(self, bot):
        """Setup some default stateful attributes to track the embeds intractive
        elements."""
        super().__init__(bot)
        self.post_text = ""
        self.embed_running = False
        self.last_embed_id = None
        self.pin_embed = False
        self.embed_pings = list()

    @commands.group(
        description="Embed Builder for Approveds!",
        case_insensitive=True,
        aliases=[
            "e",
        ],
    )
    async def embed(self, context: Context):
        """Placeholder for a grouping function. Doesnt actually do anything"""

    @embed.command(
        name="toggle-ping",
        description="Toggle if a role can be pinged",
        brief="Toggle a role",
        aliases=[
            "toggle",
        ],
    )
    @has_any_role("Admin", "Can Embed")
    async def toggle_ping(self, context: Context, *role_name):
        """Toggles a roles `mentionable` attribute
        context: Context
            The default discord Context
        role_name: str
            The name of the role to toggle"""

        if role_name:
            role_name = " ".join(role_name)

            if any(vrole.lower() in role_name.lower() for vrole in ROLES):
                role = await RoleConverter().convert(ctx=context, argument=role_name)

                if not role.mentionable:
                    await role.edit(mentionable=True)
                    await context.send(f"{role_name} is mentionable for 5 minutes.")
                else:
                    await context.send(
                        f"{role_name} was already mentionable, I will turn it off in 5 minute."
                    )
                await self._toggle_ping_off(context, role)
            else:
                await context.send("You cannot toggle that role.")
        else:
            await context.send("Please provide a Role Name!")

    @toggle_ping.error
    async def toggle_ping_error(self, ctx, err):
        """Error handler for toggle_ping"""

        if isinstance(err, discord.ext.commands.errors.RoleNotFound):
            await ctx.send(f"I couldn't find {err.argument}")
        else:
            raise err

    async def _toggle_ping_off(self, context: Context, role: Role):
        """Helper function to automatically turn roles `mentionable` attribute to False.

        Parameters
        ----------
        context: Context
            Default discord Context object.
        role: Rolel
            The discord.Role to togggle `mentionable` to False."""
        await asyncio.sleep(60 * 5)
        await role.edit(mentionable=False)
        await context.send(
            f"{role.name} has been turned off {self.bot.vegas_fingerguns}"
        )

    @embed.command(
        name="interactive-edit",
        description="Edit an Embed Interactively",
        brief="Edit an Embed Interactively",
        aliases=[
            "ie",
        ],
    )
    @has_any_role("Admin", "Mods", "Can Embed")
    async def interactive_edit(self, context, message_id: int):
        """The startup command for an interactive edit.

        Checks all text channels in the current guild for `message_id` and attempts to fetch the embed.

        Parameters
        ----------
        message_id: int
            The message id the Embed is attached to

        Notes
        -----
        - Uses self.interactive_post_loop to handle the menu'ing"""
        self.last_embed_id = message_id
        async with context.typing():
            for channel in context.message.guild.text_channels:
                try:
                    old_embed_message = await channel.fetch_message(message_id)

                    break
                except discord.NotFound:
                    pass
        try:
            old_embed = old_embed_message.embeds[0]
            embed_dict = old_embed.to_dict()
        except IndexError:
            await context.send(
                f"Message `{message_id}` does not appear to have an embed to me :(  (jump url: <{old_embed_message.jump_url}>)"
            )

            return
        except AttributeError:
            await context.send(f"I couldn't find a message with id: {message_id}")

            return
        await self.interactive_post_loop(context, embed_dict)

    @embed.command(
        name="interactive-post",
        description="Post an Embed Interactively",
        brief="Post an Embed Interactively",
        aliases=[
            "ip",
        ],
    )
    @has_any_role("Admin", "Mods", "Can Embed")
    async def interactive_post(self, context, title=None):
        """The startup command for an interactive edit.

        Checks all text channels in the current guild for `message_id` and attempts to fetch the embed.

        Parameters
        ----------
        title: str
            The title for the new embed.

        Notes
        -----
        - Uses self.interactive_post_loop to handle the menu'ing"""
        embed = discord.Embed(title=title, description="Description")
        embed_dict = embed.to_dict()
        await self.interactive_post_loop(context, embed_dict)

    async def interactive_post_loop(self, context, embed_dict):
        """The main system being all interactive functions.

        This loop provides a menu and a mapping of emoji to class methods via dict.
        Each loop the user is prompted for an emoji reaction, the bot collects that reaction,
        checks the dict mapper and runs the method on EmbedPlugin cooresponding to that emoji.

        Parameters
        ----------
        context: Context
            Default discord Context.
        embed_dict: dict
            A dictionary representing the currently edited embed."""
        self.pin_embed = False
        self.embed_pings = list()
        color = context.message.author.top_role.color.value
        embed_dict["color"] = color
        embed = discord.Embed().from_dict(embed_dict)
        embed_message = await context.send(content="Preview:", embed=embed)

        self.embed_running = True

        while self.embed_running:
            embed = embed.from_dict(embed_dict)
            await embed_message.edit(embed=embed)

            try:

                mapper = {
                    "❌": self.close_embed,
                    "💬": self.edit_desc,
                    "🖼️": self.edit_media,
                    "📦": self.edit_media,
                    "🎨": self.edit_media,
                    "👣": self.edit_footer,
                    "➕": self.add_field,
                    "🖊️": self.edit_field,
                    "💢": self.remove_field,
                    "🌈": self.change_color,
                    "📰": self.set_text,
                    "📌": self.set_pin,
                    "🏓": self.set_ping,
                    "❓": self.show_meta,
                    "🆗": self.publish,
                    "🥟": self.dump,
                }

                await embed_message.edit(
                    content=EMBED_MENU + "\nPreview (react to continue!):"
                )
                check_is_author_reaction = partial(
                    self._check_is_author_reaction_on_message_partial,
                    context,
                    embed_message,
                )
                reaction, _ = await self.bot.wait_for(
                    "reaction_add",
                    timeout=DEFAULT_TIMEOUT,
                    check=check_is_author_reaction,
                )
                menu_item = mapper.get(reaction.emoji, fallback)
                await menu_item(context, embed_message, embed_dict, reaction.emoji)
                await embed_message.clear_reactions()

            except asyncio.TimeoutError:
                self.embed_running = False
                await embed_message.edit(
                    content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                    embed=None,
                )

    async def publish(self, context, embed_message, embed_dict, *_):
        """Helper method that collects a user response and publishes the embed to the provided channel.

        Parameters
        ----------
        context: Context
            Default discord Context
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        embed_dict: dict[Any, Any]
            A dictionary representing the embed currentlying being edited.
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """
        await embed_message.edit(content="What channel would you like to publish too?")
        try:
            check_is_author_message = partial(
                self._check_is_author_message_partial, context.message.author
            )

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_is_author_message
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            embed = discord.Embed()
            try:
                channel = await TextChannelConverter().convert(
                    ctx=context, argument=message.content
                )
            except BadArgument:
                await context.send(
                    f"I couldn't find a channel matching {message.content}"
                )

                return

            embed = embed.from_dict(embed_dict)

            if self.embed_pings:
                mention_string = ""

                for role in self.embed_pings:
                    await role.edit(mentionable=True)
                    mention_string += f"{role.mention}"
                    if self.post_text:
                        mention_string += f" {self.post_text}"
                embed_message = await channel.send(content=mention_string, embed=embed)

                for role in self.embed_pings:
                    await role.edit(mentionable=False)
            else:
                if self.post_text:
                    embed_message = await channel.send(
                        content=self.post_text, embed=embed
                    )
                else:
                    embed_message = await channel.send(embed=embed)

            if self.pin_embed is True:
                await embed_message.pin()
            await message.delete()

    async def set_pin(self, *args):
        """Helper method that sets the current embed to be pinnedd on publish

        Parameters
        ----------
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """
        self.pin_embed = True
        await args[1].edit(content="This embed has been set to pin!")
        await asyncio.sleep(5)

    async def close_embed(self, *args):
        """Helper method that ends the interactive loop

        Parameters
        ----------
        context: Context
            Default discord Context
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """
        self.embed_running = False
        self.last_embed_id = None
        if self.post_text:
            await args[1].edit(content=self.post_text)
        else:
            await args[1].edit(content="-")

    async def dump(self, *args):
        """Helper method that dumps the current embed dict for review."""
        await args[0].send(args[2])

    async def edit_media(self, context, embed_message, embed_dict, emote, *_):
        """Helper method that collects a user response and edits a image and thumbnail fields on the embed

        Parameters
        ----------
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        embed_dict: dict[Any, Any]
            A dictionary representing the embed currentlying being edited.
        media_type: str
            Either 🖼️ (:framed_photo:) or 🎨(:art:) emoji.
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """

        if emote in ("📦", "🖼️"):
            media_type = "thumbnail"
        elif emote == "🎨":
            media_type = "image"

        if emote == "📦":
            embed_dict[media_type] = {}
            embed_dict[media_type]["url"] = str(context.guild.icon_url)

        else:
            await embed_message.edit(content="Post URL:")
            try:

                check_is_author_message = partial(
                    self._check_is_author_message_partial, context.message.author
                )

                message = await self.bot.wait_for(
                    "message", timeout=DEFAULT_TIMEOUT, check=check_is_author_message
                )
            except asyncio.TimeoutError:
                await embed_message.edit(
                    content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                    embed=None,
                )
            embed_dict[media_type] = {}
            embed_dict[media_type]["url"] = message.content
            await message.delete()

        if media_type == "video":
            embed_dict[media_type]["width"] = 500
            embed_dict[media_type]["height"] = 400

        return embed_dict

    async def request_num(self, context, embed_message, _):
        """Helper method that collects a the field number to be edited

        Notes
        -----
        - Used by `self.edit_field`"""
        await embed_message.edit(content="What field would you like to edit? (num):")
        try:
            check_is_author_message = partial(
                self._check_is_author_message_partial, context.message.author
            )

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_is_author_message
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            await message.delete()

            return int(message.content)

    async def add_field(self, context, embed_message, embed_dict, *_):
        """Proxy for `self.edit_field`"""
        await self.edit_field(context, embed_message, embed_dict, None)

    async def edit_field(self, context, embed_message, embed_dict, field_index):
        """Helper method that collects a user response and edits a field on the embed

        Parameters
        ----------
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        embed_dict: dict[Any, Any]
            A dictionary representing the embed currentlying being edited.
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """
        try:
            check_from_sender = partial(
                self._check_is_author_message_partial, context.message.author
            )
            await embed_message.edit(content="Set Field Name:")
            field_name = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
            await embed_message.edit(content="Set Field Value:")
            field_value = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
            await embed_message.edit(content="Set Field Inline (true/false):")
            field_inline = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_from_sender
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            if "fields" not in embed_dict:
                embed_dict["fields"] = list()

            if field_index is None:
                field_index = len(embed_dict["fields"])

            if field_index == "🖊️":
                field_index = (
                    await self.request_num(context, embed_message, embed_dict) - 1
                )

            if field_index >= len(embed_dict["fields"]):
                embed_dict["fields"].insert(field_index, dict())

            embed_dict["fields"][field_index]["name"] = field_name.content

            if field_value.content:
                embed_dict["fields"][field_index]["value"] = field_value.content
            else:
                embed_dict["fields"][field_index]["value"] = field_value.mentions
            embed_dict["fields"][field_index]["inline"] = field_inline.content
            await field_name.delete()
            await field_value.delete()
            await field_inline.delete()

            return embed_dict

    async def remove_field(self, context, embed_message, embed_dict, *_):
        """Helper method that collects a user response and removes a field from the embeds

        Parameters
        ----------
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        embed_dict: dict[Any, Any]
            A dictionary representing the embed currentlying being edited.
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """
        await embed_message.edit(
            content="What field do you want to remove? (numbers only)"
        )
        try:
            check_is_author_message = partial(
                self._check_is_author_message_partial, context.message.author
            )

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_is_author_message
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )

        mindex = int(message.content)

        if mindex <= len(embed_dict["fields"]):
            del embed_dict["fields"][int(message.content) - 1]
        else:
            await embed_message.edit(
                content="**I don't think there are that many fields on this embed...**"
            )
            await asyncio.sleep(5)
        await message.delete()

        return embed_dict

    async def edit_footer(self, context, embed_message, embed_dict, *_):
        """Helper method that collects a user response and updates the embeds `footer` atttribute

        Parameters
        ----------
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        embed_dict: dict[Any, Any]
            A dictionary representing the embed currentlying being edited.
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """
        await embed_message.edit(content="Set Footer Text:")
        try:
            check_is_author_message = partial(
                self._check_is_author_message_partial, context.message.author
            )

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_is_author_message
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            if "footer" not in embed_dict:
                embed_dict["footer"] = dict()
            embed_dict["footer"]["text"] = message.content
            await message.delete()

            return embed_dict

    async def edit_desc(self, context, embed_message, embed_dict, *_):
        """Helper method that collects a user response and updates the embeds `description` atttribute

        Parameters
        ----------
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        embed_dict: dict[Any, Any]
            A dictionary representing the embed currentlying being edited.
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """
        await embed_message.edit(content="Set description:")
        try:
            check_is_author_message = partial(
                self._check_is_author_message_partial, context.message.author
            )

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_is_author_message
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            embed_dict["description"] = message.content
            await message.delete()

            return embed_dict

    async def show_meta(self, *args):
        """Helper function that displays meta information about the embed that is not stored on the dict.

        The information displayed as of now is if the embed should be pinned and if any roles should be pinged.
        """
        pinning = self.pin_embed or "Not Pinning"
        pinging = ""

        if self.embed_pings:
            for role in self.embed_pings:
                pinging += f"{role.mention} "
        else:
            pinging = "Not Pinging any Roles"
        await args[1].edit(content=f"*Pin:* {pinning}\n*Pinging:* {pinging}")
        await asyncio.sleep(10)

    async def set_text(self, context: Context, embed_message: Message, *_):
        """Helper method to collect text to send on publish."""
        await embed_message.edit(
            content=(
                "What message would you like posted with this Embed?\n"
                "(*Note:* Accepts between 1 - 4090 characters.)"
            )
        )
        content = await MessageReply(ctx=context, validate=r"^[\s\S]{,4090}$").result()
        self.post_text = content.content

    async def set_ping(self, context: Context, embed_message: Message, *_):
        """Helper method to set `self.embed_pings`, which results in a role ping on publish.

        Parameters
        ----------
        context: Context
            Default discord Context
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        *_: List[Any]
            A catch all of all other parameters. This allows the method to fit an pattern.
        """
        self.embed_pings = list()
        await embed_message.edit(content="Set Ping for Publish:")
        try:

            check_is_author_message = partial(
                self._check_is_author_message_partial, context.message.author
            )

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_is_author_message
            )
        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity,",
                embed=None,
            )
        else:
            if message.content:
                ping_roles = message.content.split(",")
                roles_mentions = ""

                for role_lookup in ping_roles:
                    try:
                        role = await RoleConverter().convert(
                            ctx=context, argument=role_lookup
                        )
                        roles_mentions += f"{role.mention} "

                        if any(
                            ch_role.lower() in role.name.lower() for ch_role in ROLES
                        ):
                            self.embed_pings.append(role)
                        else:
                            await embed_message.edit(
                                content=f"{role.mention} isn't a role you're allowed to ping with this module."
                            )
                    except BadArgument:
                        await embed_message.edit(
                            content=f"I can't find role: `{message.content}`."
                        )

                        return
                await embed_message.edit(
                    content=f"When published, this embed will ping {roles_mentions}"
                )
            await message.delete()
            await asyncio.sleep(5)

    async def change_color(
        self, context: Context, embed_message: Message, embed_dict, *_
    ):
        """Helper function to change the `color` attribute of an embed.

        Parameters
        ----------
        context: Context
            Default discord Context
        embed_message: Message
            The Message that is currentlying being edited by the interactive process.
        embed_dict: dict
            A dictionary representing the current embed being edted."""
        await embed_message.edit(content="Set Color (hex without #):")
        try:
            check_is_author_message = partial(
                self._check_is_author_message_partial, context.message.author
            )

            message = await self.bot.wait_for(
                "message", timeout=DEFAULT_TIMEOUT, check=check_is_author_message
            )

        except asyncio.TimeoutError:
            await embed_message.edit(
                content="Your session timed out due to inactivity! Thanks for using VEGAS!",
                embed=None,
            )
        else:
            embed_dict["color"] = int(message.content, 16)
            await message.delete()

            return embed_dict


def setup(bot):
    """Simple setup for Cog loader"""
    bot.add_cog(EmbedPlugin(bot))
