from typing import Optional

import discord
from discord import Embed
from discord.ext import commands
from discord.ext.commands import Context
from settings.base import (
    COLORS,
    INTERESTS_DICT,
    INTERESTS_DICT1,
    RULES_BODY,
    RULES_TRANSFER,
)

from .base import BasePlugin

keywords = "\n".join(INTERESTS_DICT.keys())
searchterms = INTERESTS_DICT.keys()

TONE_INDICATOR_DICT = {
    "/srs": "Serious",
    "/nsrs": "Not Serious",
    "/p": "Platonic",
    "/t": "Teasing",
    "/nm": "Not Mad",
    "/lu": "Little Upset",
    "/neg or /nc": "Negative Connotation",
    "/pos or /pc": "Positive Connotation",
    "/lh": "Light Hearted",
    "/nbh": "Nobody Here",
    "/m": "Metaphorically",
    "/li": "Literally",
    "/ij": "Inside Joke",
    "/j": "Joke/Joking",
    "/hj": "Half Joking",
    "/rh or /rt": "Rhetorical Question",
    "/gen or /g": "Genuine (Not Sarcastic)",
    "/s": "Sarcastic",
    "/hyp": "Hyperbole",
}


class InfoPlugin(BasePlugin):
    @commands.group(aliases=["info", "i"])
    async def information(self, context):
        if context.invoked_subcommand is None:
            pass

    @information.command(
        description="Does MatPat have a discord?",
        case_insensitive=True,
        aliases=["mpd", "matpat", "matpat-discord"],
    )
    async def matpat_discord(self, context):
        embed = discord.Embed(
            title="Does Matpat Have a Discord?",
            description="As the [first message](https://discord.com/channels/353694095220408322/483967686540394506/735637644087656518) in <#483967686540394506> states, MatPat does not have Discord account. We are a community made and run server that MatPat has endorsed in his videos. For reaching out about theories and game suggestions, you should head to his [Twitter](https://twitter.com/MatPatGT) or use the [Reddit Megathread](https://www.reddit.com/r/GameTheorists/comments/lezw1o/theory_suggestions_megathread/) instead.",
            color=context.author.top_role.color,
        )
        await context.send(embed=embed)

    @information.command(
        description="Looks like someone needs a refresher.",
        case_insensitive=True,
        aliases=["si", "server-info"],
    )
    async def server_info(self, context):
        embed = discord.Embed(
            title="#server-info Index",
            description="[Events](https://discord.com/channels/353694095220408322/735635802960429087/735635967209373756)\n[Roles](https://discord.com/channels/353694095220408322/735635802960429087/735636045143474308)\n[Leveled Roles](https://discord.com/channels/353694095220408322/735635802960429087/735636099409641504)\n[Leveled Roles, cont.](https://discord.com/channels/353694095220408322/735635802960429087/735639337613656086)\n[Approved Roles](https://discord.com/channels/353694095220408322/735635802960429087/735639445772042260)\n[Approved Roles, cont.](https://discord.com/channels/353694095220408322/735635802960429087/735639476222558209)\n[Miscellaneous](https://discord.com/channels/353694095220408322/735635802960429087/735639540022247477)",
            color=context.author.top_role.color,
        )
        await context.send(embed=embed)

    @information.command(
        description="Looking for something in the server? Try here!",
        case_insensitive=True,
        aliases=["mii", "my-interest-is", "interest"],
    )
    async def my_interest_is(self, context, *interest):
        interest = " ".join(interest)

        if interest:
            if "@everyone" in interest:
                await context.send(
                    "Mass ping detected--please try again without 'everyone'."
                )
            elif "@here" in interest:
                await context.send(
                    "Mass ping detected--please try again without 'here'."
                )
            else:
                for broad_Term in INTERESTS_DICT1:
                    if interest in INTERESTS_DICT1[broad_Term]:
                        interest = broad_Term

                if interest.lower() in searchterms.lower():
                    embed = discord.Embed(
                        title="My Interest is {}!".format(interest),
                        description="Here are the results for your interest:",
                        color=context.author.top_role.color,
                    )
                    embed.add_field(
                        name=interest.title(),
                        value=INTERESTS_DICT[interest],
                        inline=False,
                    )
                else:
                    embed = discord.Embed(
                        title="My Interest is Unknown!",
                        description="Seems like you need some help with the search terms. Provide it like this: `v!i my-interest-is My Interest`.",
                        color=context.author.top_role.color,
                    )
                    embed.add_field(
                        name="List of Keywords:", value=keywords, inline=False
                    )
                await context.send(embed=embed)
        else:
            embed = discord.Embed(
                title="My Interest is Unknown!",
                description="Seems like you need some help with the search terms. Provide it like this: `v!i my-interest-is My Interest`.",
                color=context.author.top_role.color,
            )
            embed.add_field(name="List of Keywords:", value=keywords, inline=False)
            await context.send(embed=embed)

    @information.command(
        description="Looks like someone needs a refresher.",
        case_insensitive=True,
        aliases=["rules", "r", "ttr", "thems-the-rules", "rule"],
    )
    async def thems_the_rules(self, context, rule=None):
        if rule:
            if rule in RULES_TRANSFER:
                rule = RULES_TRANSFER[rule]
                embed = discord.Embed(
                    title=rule,
                    description=RULES_BODY[rule],
                    color=context.author.top_role.color,
                )
                embed.add_field(
                    name="Punishments and Penalties",
                    value="[This explains the warning and penalty system.](https://discord.com/channels/353694095220408322/483967686540394506/735638956095438848)",
                    inline=False,
                )
                await context.send(embed=embed)
            else:
                await context.send(
                    "You haven't given me an existing rule number. :pensive:"
                )

        else:
            await context.send("You haven't given me a rule to check. :pensive:")

    @information.command(
        description="Explains where the roles are explained in detail.",
        case_insensitive=True,
        aliases=[],
    )
    async def roles(self, context):
        embed = discord.Embed(
            title="What roles are there, and where can I get them?",
            description="The leveled roles are explained in <#735635802960429087>, the roles you can request are in the pinned messages in <#560538258115919872> and the ones you can assign yourself are in <#377304867686842368>.",
            color=context.author.top_role.color,
        )
        await context.send(embed=embed)

    @information.command(
        description="Reminds everyone to Turn off the Pings on replies",
        case_insensitive=True,
        aliases=["turn_off_ping", "ping"],
    )
    @commands.cooldown(1, 10.0, commands.BucketType.guild)
    async def turn_off_the_ping(self, context):
        await context.send(
            "https://cdn.discordapp.com/attachments/762118319632809984/844586009076826142/turn-off-the-ping-please.gif"
        )

    @information.command(
        description="Gives a brief explaination of tone indicators",
        case_insensitive=True,
        aliases=[
            "tone-indicator",
            "tone",
            "gen",
            "nm",
            "pos",
        ],
    )
    async def tone_indicator(self, context: Context, indicator: Optional[str]):
        """Provides an embed that explains tone indicators.

        Accepts a tone indcator to search. For example `/s`.

        Parameters
        ==========
        indicator: Optional[str]
            The tone indicator to search by.
        """
        all_indicators = "\n".join(f"{k} - {v}" for k, v in TONE_INDICATOR_DICT.items())

        if indicator:
            indicators = TONE_INDICATOR_DICT.get(indicator.lower(), None)

            if indicators:
                indicators = f"{indicator} - {indicators}"

            else:
                indicator = all_indicators
        else:
            indicators = all_indicators

        nl = "\n"
        description = f"Interpreting messages online can be hard! A lot of people struggle with issues like 'are they being sarcastic?' or 'that feels like they are mocking me...' because the \"tone\" or intent of a message was mis-read. {nl}{nl}Tone Indicators help with that! {nl}{nl}Just add an indicator to end of a message to help better express what you mean! /pos{nl}"
        embed = Embed(
            title="What are Tone Indicators?",
            description=description,
            color=COLORS["ban"],
        )
        embed.add_field(
            name="Indicators",
            value=(indicators),
            inline=False,
        )
        embed.set_thumbnail(url=context.guild.icon_url)

        await context.send(embed=embed)

def setup(bot):
    bot.add_cog(InfoPlugin(bot))
