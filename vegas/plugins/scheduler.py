import asyncio
import datetime
from functools import partial
from typing import Union
import uuid
import logging
from functools import wraps

import aiocron
from discord import Embed, Member, Message, TextChannel
from discord.ext import commands
from discord.ext import commands, tasks
from discord.ext.commands import Context, MissingRequiredArgument
import pytz
from tzlocal import get_localzone

from checks import requires_redis
from checks import setup_check_redis

DEFAULT_TIMEZONE = "America/New_York"
TIMEZONE = pytz.timezone(DEFAULT_TIMEZONE)
FMT = "%m-%d-%Y %I:%M %p"

logging.basicConfig(level=logging.INFO)


class SchedulerPlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.scheduled_crons = list()
        self.redis_scheduler_key = "{guild_id}:scheduler"
        self.redis_scheduled_jobs_key = "scheduler:{uuid}:{timestamp}:{func_name}"
        self._load_lock = asyncio.Lock()
        self._load_scheduled_jobs.start()

    @tasks.loop(count=1.0)
    async def _load_scheduled_jobs(self):
        async with self._load_lock:

            for guild in self.bot.guilds:
                schedule_list = await self.bot.redis.lrange(self.redis_scheduler_key.format(guild_id=guild.id), 0, -1, encoding="utf-8")

                if not schedule_list:
                    continue

                for job in schedule_list:
                    await self._set_guild_schedule(job)

    async def _set_guild_schedule(self, job: str):
        _, _, timestamp, func_name = job.split(":")
        channel_id, message_info = await self.bot.redis.lrange(job, 0, -1, encoding="utf-8")
        to_channel = self.bot.get_channel(int(channel_id))
        message_channel_id, message_id = message_info.split(",")
        message_channel = self.bot.get_channel(int(message_channel_id))
        message = await message_channel.fetch_message(int(message_id))
        try:
            dt = datetime.datetime.fromtimestamp(int(timestamp[:-2]))
            cron_format_time = datetime_to_cron(dt)
        except ValueError:
            cron_format_time = dt = timestamp
        context = await self.bot.get_context(message)
        future_post = partial(post, message, to_channel, context, dt)
        setattr(future_post.func, "message", message)
        setattr(future_post.func, "context", context)
        setattr(future_post.func, "to_channel", to_channel)
        setattr(future_post.func, "next_job", dt)
        print(cron_format_time)
        cron = aiocron.crontab(cron_format_time, func=future_post, start=True)
        self.scheduled_crons.append(cron)

    def cog_unload(self):
        """Loop through and end all aiocron jobs when the cog unloads.
        If this does not occur, the cronjobs will continue to run indefinitely."""
        for cron_job in self.scheduled_crons:
            cron_job.stop()

    @commands.group()
    async def schedule(self, context: Context):
        """Base group command for scheduling."""
        if context.invoked_subcommand is None:
            await context.send("Please use a subcommand.")

    @schedule.command()
    async def post(
        self,
        context: Context,
        cron_time: str,
        message: Message,
        to_channel: TextChannel,
    ):
        """Schedule a `message:Message` to be posted at a future `cron_time:str`.
        Parameters
        ==========
        cron_time: str
            The date and time the message should be sent. Expects `MM-DD-YYYY HH:MM` check schedul tzo for timezone information.
        message: Message
            The message that should be posted at the given time.
        to_channel: TextChannel
            The channel the given message should post to."""
        dt = await validate_datetime(cron_time)
        scheduler_key = self.redis_scheduler_key.format(guild_id=context.guild.id)
        scheduler_job_key = self.redis_scheduled_jobs_key.format(uuid=uuid.uuid4(), timestamp=datetime.datetime.timestamp(dt), func_name="post")
        cron_format_time = datetime_to_cron(dt)
        future_post = partial(post, message, to_channel, context, dt)
        setattr(future_post.func, "message", message)
        setattr(future_post.func, "context", context)
        setattr(future_post.func, "to_channel", to_channel)
        setattr(future_post.func, "next_job", dt)
        cron = aiocron.crontab(cron_format_time, func=future_post, start=True)
        self.scheduled_crons.append(cron)
        await self.bot.redis.lpush(scheduler_key, scheduler_job_key)
        await self.bot.redis.lpush(scheduler_job_key, f"{message.channel.id},{message.id}", to_channel.id)
        await context.send("Job Set!")

    @post.error
    async def post_error(self, context, error):
        """Error handler for post."""
        if isinstance(error, MissingRequiredArgument):
            if error.param.name == "cron_time":
                await context.send(
                    "We need a `cron_time` in the form of `MM-DD-YYYY HH:MM AM|PM`!"
                )
            if error.param.name == "message":
                await context.send("We need a `message` in the form of a message ID!")
            if error.param.name == "to_channel":
                await context.send("We need a `to_channel` in the form of a mention!")

    @schedule.command()
    async def raw_post(
        self,
        context: Context,
        cron_time: str,
        message: Message,
        to_channel: TextChannel,
    ):
        """Schedule a `message:Message` to be posted at a future `cron_time:str`. Accepts raw cron formats.
        Parameters
        ==========
        cron_time: str
            The date and time the message should be sent. Expects `MM-DD-YYYY HH:MM` check schedul tzo for timezone information.
        message: Message
            The message that should be posted at the given time.
        to_channel: TextChannel
            The channel the given message should post to."""
        future_post = partial(post, message, to_channel, context, cron_time)
        setattr(future_post.func, "message", message)
        setattr(future_post.func, "context", context)
        setattr(future_post.func, "to_channel", to_channel)
        setattr(future_post.func, "next_job", cron_time)
        cron = aiocron.crontab(cron_time, func=future_post, start=True)
        cron.func.__name__ = f"Raw Cron Post added by {context.author.mention}"
        self.scheduled_crons.append(cron)
        scheduler_key = self.redis_scheduler_key.format(guild_id=context.guild.id)
        scheduler_job_key = self.redis_scheduled_jobs_key.format(uuid=uuid.uuid4(), timestamp=cron_time, func_name="post")
        await self.bot.redis.lpush(scheduler_key, scheduler_job_key)
        await self.bot.redis.lpush(scheduler_job_key, f"{message.channel.id},{message.id}", to_channel.id)
        await context.send("Raw Job Set!")

    @schedule.command()
    async def list(self, context: Context):
        """List all scheduled jobs."""
        await context.send(f"{self.bot.user.mention} timezone is {DEFAULT_TIMEZONE}")
        embed = Embed(
            title="Active Jobs", description="Below are the currently schedule jobs."
        )
        logging.info("Starting Prune---")
        await self.prune_jobs()
        logging.info("---Ended Prune")

        for i, cron_job in enumerate(self.scheduled_crons):
            i += 1
            job_type = cron_job.func.func.__name__
            tzfmt = FMT + " %Z [UTC %z]"
            next_job = cron_job.func.func.next_job
            to_channel = cron_job.func.func.to_channel
            orig_context = cron_job.func.func.context
            orig_message = cron_job.func.func.message

            if "post" in job_type:
                if isinstance(next_job, datetime.datetime):
                    print("Is Datetime!")
                    next_job = "Scheduled At: " + next_job.astimezone(
                        TIMEZONE
                    ).strftime(tzfmt)
                else:
                    print(f"Not Datetime! {next_job}")
                    next_job = f"Cron FMT: {next_job}"
                ename = "Post Messagee"
                eval = (
                    f"Job Id: {i}"
                    "\n"
                    f"Scheduled by {orig_context.author.mention}"
                    "\n"
                    f"{next_job}"
                    "\n"
                    f"Channel: {to_channel.mention}"
                    "\n"
                    f"[Message being Posted]({orig_message.jump_url})"
                )
            embed.add_field(name=ename, value=eval, inline=False)

        embed.set_thumbnail(url=context.guild.icon_url)
        await context.send(embed=embed)

    @schedule.command(aliases=["tz_offest", "tzo"])
    async def timezone_offset(self, context: Context):
        """Check the bots timezone and a useful URL to convert timezones."""
        await context.send(f"{self.bot.user.mention} this its in {get_localzone()}")
        await context.send(
            "You can use this website to convert timezones: https://www.timeanddate.com/worldclock/converter.html"
        )

    @schedule.command(aliases=["delete", "d"])
    async def delete_job(self, context: Context, job_id: int):
        real_job_id = job_id - 1
        del self.scheduled_crons[real_job_id]
        await context.send("Deleted job {job_id}")

    async def prune_jobs(self):
        current_jobs = list()
        for cron_job in self.scheduled_crons:
            orig_func = cron_job.func.args
            try:
                next_job = orig_func.next_job.astimezone(TIMEZONE)
                now_tz = datetime.datetime.now(TIMEZONE)
                if next_job > now_tz:
                    current_jobs.append(cron_job)
            except AttributeError:
                current_jobs.append(cron_job)
        self.scheduled_crons = current_jobs


def setup(bot):
    setup_check_redis(bot, __file__)
    bot.add_cog(SchedulerPlugin(bot))


async def validate_datetime(cron_time: str):
    try:
        given_time = datetime.datetime.strptime(cron_time, FMT)
        vegas_timezone = TIMEZONE.localize(given_time)
        cron_timezone = vegas_timezone.astimezone(pytz.timezone("UTC"))
        return cron_timezone

    except ValueError:
        return False


def datetime_to_cron(date) -> str:
    """Converts a datetime.datetime object into a cron parsable string.
    Parameters
    ==========
    date: datetime.datetime
        A datetime object representing the date/time to be converted."""

    # return f"{date.minute} {date.hour} {date.day} {date.month} {int(date.weekday())+1}"
    return f"{date.minute} {date.hour} {date.day} {date.month} *"


# def annotate_function():
#     def decorator(func):
#         @wraps(func)
#         def wrapper(*args, **kwargs):
#             logging.info(args)
#             logging.info(kwargs)
#             #setattr(func, )
#             return func(*args, **kwargs)
#         # args = wrapper.args
#         # func.message = args[0]
#         # func.to_channel = args[1]
#         # func.context = args[2]
#         # func.next_job = args[3]
#         return wrapper
#     return decorator


# @annotate_function()
async def post(
    message: Message,
    to_channel: TextChannel,
    context: Context,
    dt: Union[datetime.datetime, str],
):
    """A Partial function to be used in SchedulerPlugin.post.

    Parameters
    ==========
    message: Message
        A reference to the message to be posted.
    to_channel: TextChannel
        The channel to post the message to.
    dt: datetime.datetime
        The time post should be sent for quick reference."""
    send_files = list()
    embed = None
    if message.attachments:
        for attachment in message.attachments:
            attachment = await attachment.to_file()
            send_files.append(attachment)
    if message.embeds:
        embed = message.embeds[0]
    if message.role_mentions:
        for role in message.role_mentions:
            if not role.mentionable:
                pass
                await role.edit(mentionable=True)
    await to_channel.send(
        content=f"{message.content}",
        embed=embed,
        files=send_files,
    )
    if message.role_mentions:
        for role in message.role_mentions:
            if role.mentionable:
                await role.edit(mentionable=False)
