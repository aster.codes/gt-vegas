"""The primary class that makes up the VEGAS bot."""
import asyncio
import datetime
import os
import sys
import traceback

from blurple import ext
from discord import Forbidden, Game, Intents
from discord.ext.commands import (
    Bot,
    CommandInvokeError,
    ExtensionFailed,
    ExtensionNotFound,
    NoEntryPointError,
)
from discord.ext.commands.errors import MissingAnyRole
from httpx import AsyncClient
from jishaku.help_command import MinimalEmbedPaginatorHelp

from exceptions.setup import PostgresNotInstalled, RedisNotInstalled
from exceptions.suggestions import NotVotingGuild
from settings.base import POSTGRES_ERR, REDIS_ERR, VOTING_GUILD_ID
from settings.local_settings import (
    BOT_PREFIX,
    COGS,
    DEBUG,
    MAX_MESSAGES,
    setup_postgres_pool,
    setup_redis,
)


class VEGAS(Bot):
    """Base VEGAS bot"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pg_pool = None
        self.redis = None
        self.vegas_fingerguns = None
        self.start_time = datetime.datetime.now()
        os.environ["TZ"] = "America/New_York"

        self.httpx = AsyncClient()
        self.loop.run_until_complete(setup_redis(self))
        self.loop.run_until_complete(setup_postgres_pool(self))
        self.load_extension("jishaku")

        for cog in COGS:
            try:
                self.load_extension(cog)
            except (ExtensionNotFound, ExtensionFailed, NoEntryPointError):
                print(f"Failed to load extension {cog}.", file=sys.stderr)
                traceback.print_exc()

    async def logout(self):
        if hasattr(self, "pg_pool"):
            self.pg_pool.close()

        if hasattr(self, "redis"):
            self.redis.close()
        super.logout()

    async def on_ready(self):
        """Default startup command"""
        print("Bot on_ready()")
        await client.change_presence(activity=Game(name="with development"))
        self.vegas_fingerguns = next(
            filter(lambda emoji: emoji.name == "VegasFingerguns", self.emojis)
        )
        print("Logged in as " + client.user.name)

    async def on_error(self, _, ctx, error, **kwargs):
        """Default Error Handling for whole bot.
        If your method/cog needs a custom error message,
        create a custom Exception, and match against it here!

        Parameters
        ----------
        _: event
            The event being dispatched
        error: Exception
            The exception bieng raised"""

        if hasattr(ctx.command, "on_error"):
            return

        if isinstance(error, MissingAnyRole):
            roles_list_str = "\n - ".join(error.missing_roles)
            await ctx.send(
                f"Sorry {ctx.message.author.mention}! You need one of the following roles to use that command! {roles_list_str}"
            )
        elif isinstance(error, PostgresNotInstalled):
            err = f"{ctx.cog.qualified_name}: {POSTGRES_ERR}"

            if DEBUG:
                print(err)
            else:
                ctx.guild.owner.send(err)
            await ctx.send(err)
        elif isinstance(error, RedisNotInstalled):
            err = f"{ctx.cog.qualified_name}: {REDIS_ERR}"

            if DEBUG:
                print(err)
            else:
                ctx.guild.owner.send(err)
            await ctx.send(err)
        else:
            print(error)

    async def on_command_error(self, context, error):
        print(error)
        if isinstance(error, NotVotingGuild):
            await context.send(
                f"This command can only be run in {VOTING_GUILD_ID}, which is defined in `settings.base`."
            )
        elif isinstance(error, MissingAnyRole):
            roles_list_str = "\n - ".join(error.missing_roles)
            await context.send(
                (
                    f"Sorry {context.message.author.mention}! You need one "
                    "of the following roles to use that command! "
                    f"{roles_list_str}"
                )
            )
        elif isinstance(error, CommandInvokeError):
            if isinstance(error.original, Forbidden):
                await context.send(
                    "Sorry! It doesn't appear that I have permissions to do that..."
                )
        else:
            print(error)
            print(type(error))


intents = Intents.default()
intents.members = True

if sys.platform == "win32":
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

client = VEGAS(
    messages=MAX_MESSAGES,
    command_prefix=BOT_PREFIX,
    case_insensitive=True,
    intents=intents,
)
client.help_command = MinimalEmbedPaginatorHelp()
